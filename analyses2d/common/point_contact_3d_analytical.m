function [u, v, w, sxx, syy, szz, sxy, sxz, syz] = point_contact_3d_analytical(x, y, z, P, E, nu)

r = sqrt(x.^2+y.^2);
R = sqrt(x.^2+y.^2+z.^2);
ct = x ./ r;
st = y ./ r;

mu = E / (2*(1+nu));

ur = P*r/(4*pi*mu) .* (...
  z ./ R.^3 - (1-2*nu) ./ (R.*(z+R))...  
);

w = P/(4*pi*mu) .* (...
  z.^2 ./ R.^3 + 2*(1-nu) ./ R...  
); 

u = ur.*ct;
v = ur.*st;

srr = P/(2*pi)*((1-2*nu)./ (R.*(R+z)) - 3*r.^2.*z./R.^5);
szz = -3*P/(2*pi)*z.^3./R.^5;
stt = P/(2*pi)*(1-2*nu)*(z./R.^3 - 1 ./ (R.*(R+z)));
srz = -3*P/(2*pi)*r.*z.^2./R.^5;

sxx = srr.*ct.^2 + stt.*st.^2;
sxy = (srr-stt).*ct.*st;
sxz = srz.*ct;
syy = stt.*ct.^2 + srr.*st.^2;
syz = srz.*st;

end
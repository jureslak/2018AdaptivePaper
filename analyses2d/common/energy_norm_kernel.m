function [ e ] = energy_norm_kernel(sxx, syy, sxy, lam, mu) 
% ENERGY_NORM evaluates sigma : C^-1 : sigma.
% lam and mu are Lame parameters
f = 4*mu*(lam+mu);
e = (lam*(2*sxy.^2 + (sxx-syy).^2) + 2*mu*(sxx.^2+sxy.^2+syy.^2)) / f;
end
function x = linf_norm(a, b, c)
if nargin == 1
    x = max(abs(a));
elseif nargin == 2
    x = max([max(abs(a)) max(abs(b))]);
elseif nargin == 3
    x = max([max(abs(a)) max(abs(b)) max(abs(c))]);
end    
end
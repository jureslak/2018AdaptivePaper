function [ e ] = energy_norm_kernel3(sxx, syy, szz, sxy, sxz, syz, lam, mu)
% ENERGY_NORM evaluates sigma : C^-1 : sigma.
% lam and mu are Lame parameters

f = 3*mu*(3*lam + 2*mu);
e = sxy.^2/mu + sxz.^2/mu + syz.^2/mu + ...
 sxx.*(sxx/(2*mu) - (lam*(sxx + syy + szz))/f) + ...
 syy.*(syy/(2*mu) - (lam*(sxx + syy + szz))/f) + ...
 szz.*(szz/(2*mu) - (lam*(sxx + syy + szz))/f);

end

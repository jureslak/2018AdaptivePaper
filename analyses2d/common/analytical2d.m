function [z] = analytical2d(x, y, a, b)
z = sin(a*pi*x).*sin(b*pi*y);
end


clear
close all
datapath = '/mnt/data/ijs/papers/2018Adaptive/analyses/';
plotdatapath = 'plotdata/';
paramspath = 'params/';
imagepath = 'images/';
exportpath = 'exportdata/';
format compact
include "colors.idp"  // colormaps for nicer output
include "utilities.idp"
load "Element_P3"           // P3 Finite Elements

// http://www-e6.ijs.si/ParallelAndDistributedSystems/MeshlessMachine/
// wiki/index.php/Hertzian_contact#Phsyical_parameters

// Basic parameters
real E = 72.1e9;          // Young modulus
real nu = 0.33;           // Poisson ratio
real F = 543;             // Normal force
real Q = 155.165;             // Tangential force
real sigmaAxial = 100e6; // bulk axial stress
real cof = 0.3;           // coefficient of friction
real R = 10.0e-3;           // cylinder radius
real length = 40.0e-3;      // specimen length
real height = 5.0e-3;      // specimen height
real thickness = 4.0e-3;      // specimen thickness
real halflength = length/2;

// Derived parameters
real Ec = E/(2*(1-nu^2));   // combined modulus of elasticity
real pmax = sqrt(F*Ec/(thickness*pi*R));
real a = 2.0*sqrt(F*R/(thickness*pi*Ec));
real c = a*sqrt(1-abs(Q/(cof*F)));
real ee = a*sigmaAxial/(4*cof*pmax);

// Print parameters to screen for inspection
cout << "E^* = " << Ec << endl;
cout << "pmax = " << pmax << endl;
cout << "a = " << a << endl;
cout << "c = " << c << endl;
cout << "e = " << ee << endl;
if ((c + ee) <= a){
    cout << "c + e <= a is TRUE" << endl;
}
else{
    cout << "c + e <= a is FALSE" << endl;
}

// Lame coefficients
real mu = E/(2*(1+nu));
real lambda = E*nu/((1+nu)*(1-2*nu));



// Mesh adaptation
bool adapt = true;
int niter = 30;

// Boundary labels
int symm = 1;
int free = 2;
int contact = 3;
int fixed = 4;

// Initial mesh
int Nx = 100;
int Ny = 10;
mesh Th = square(Nx,Ny,[-halflength+length*x,-height+height*y]);
plot(Th,wait=0);

// Macro definitions
macro u [ux,uy] //
macro v [vx,vy] //
macro stress [sxx,syy,sxy] //
macro e(u) [dx(u[0]),dy(u[1]),(dx(u[1])+dy(u[0]))/2] //
macro em(u) [dx(u[0]),dy(u[1]),sqrt(2)*(dx(u[1])+dy(u[0]))/2] //
macro D [[2*mu+lambda,lambda,0],[lambda,2*mu+lambda,0],[0,0,2*mu]] //
macro force [fx,fy] //

func real Pressure(real r, real a, real pmax){
    if (abs(r) < a){
        return -pmax*sqrt(1.0-(r/a)^2);
    }
    else{
        return 0.;
    }
}

func real ShearTraction(real xx, real a, real c, real cof, real pmax)
{
    if (abs(xx) < c)
    {
        return -cof*pmax*(sqrt(1.-(xx/a)^2) - c/a*sqrt(1.-(xx/c)^2));
    }
    else if (abs(xx) >= c & abs(xx) <= a)
    {
        return -cof*pmax*sqrt(1.-(xx/a)^2);
    }
    else
    {
        return 0.;
    }
}

func real ShearTractionBulkLoad(real xx, real a, real c, real ee, real cof, real pmax)
{
    //if ((xx >= (c - ee) & xx <= a) | (xx <= -(c+ee) & xx >= -a))
    if ((abs(xx) <= a) & (c <= abs(xx+ee)))
    {
        return -cof*pmax*sqrt(1 - (xx/a)^2);
    }
    else if (abs(xx + ee) < c)
    {
        return -cof*pmax*(sqrt(1 - (xx/a)^2) - c/a*sqrt(1 - ((xx + ee)/c)^2));
    }
    else
    {
        return 0;
    }
}

func real ShearTractionBulkLoadv2(real xx, real a, real c, real ee, real cof, real pmax)
{
    real res;
    if (abs(xx) < a)
    {
        res = sqrt(1 - (xx/a)^2);
        if (abs(xx + ee) < c)
        {
            res -= c/a*sqrt(1 - ((xx + ee)/c)^2);
        }
    }
    else
    {
        return 0;
    }
    res *= -cof*pmax;
    return res;
}

//func fx = 0;
//func fx = ShearTraction(x,a,c,cof,pmax);
//func fx = ShearTractionBulkLoad(x,a,c,ee,cof,pmax);
func fx = ShearTractionBulkLoadv2(x,a,c,ee,cof,pmax);
func fy = Pressure(x,a,pmax);

fespace Sh(Th,P1);
fespace Vh(Th,[P2,P2]);
fespace Qh(Th,[P1,P1,P1]);
Vh u,v;
Qh stress;

// Weak form of elastostatic equation
problem CylinderContact(u,v) =
    int2d(Th)((D*em(u))'*em(v))
    - int1d(Th,contact)(force'*v)
    - int1d(Th,free)([sigmaAxial, 0]'*v)
    + on(fixed,ux=0,uy=0)
    + on(symm,uy=0);

// Solve with mesh adaptation or without
if (adapt)
{
    real error = 0.1;
    for (int i=0;i<niter;i++)
    {
        plot(Th,wait=0);
        CylinderContact;
        error = 0.85*error;
        Th = adaptmesh(Th,u,err=error,cutoff=1e-8,ratio=1.2,iso=1,splitin2=true);
        cout << "i = " << i << endl;
        cout << "Degrees of freedom = " << Vh.ndof/2 << endl;
    }
    Th = trunc(Th,1,split=2);
    cout << "Degrees of freedom = " << Vh.ndof/2 << endl;
    plot(Th,wait=0);
    CylinderContact;
}
else
{
    CylinderContact;
}

// Stresses and principal stress difference
stress = D*e(u);

Sh psd,vms,mss;
psd = sqrt((sxx-syy)^2 + 4*sxy^2);
mss = sqrt((sxx-syy)^2/4 + sxy^2);
vms = sqrt(sxx^2 - sxx*syy + syy^2 + 3.*sxy^2);

Sh press,shear;
press = fy;
shear = fx;
printLine5("fwo_surface.dat",[(-3*a),0],[3*a,0],2000,sxx,syy,sxy,press,shear);
printLine3("fwo_symmetry.dat",[0,-height],[0,0],200,sxx,syy,sxy);

{
    mesh Tho = trunc(Th,1,split=2);
    fespace Sho(Tho,P1);
    Sho sx = sxx, sy = syy, tau = sxy;
    Sho u1 = ux, u2 = uy;
    savemesh(Tho,"fwo_mesh.msh");
    printTriField2("fwo_displacement.dat",Sho,[u1,u2]);
    printTriField3("fwo_stress_field.dat",Sho,[sx,sy,tau]);
}

// Displacement vectors
real coef = 10;
//plot(Th,u,wait=1,coef=coef);

// Stress difference field
// plot(sxx,wait=1,value=1,fill=1,hsv=RdBu);

// Displaced mesh
//real minT0 = checkmovemesh(Th,[x,y]);
//while(1)
//{
//    real minT1 = checkmovemesh(Th,[x+ux*coef,y+uy*coef]);
//    if (minT1 > minT0/5) break;
//    coef /= 1.5;
//}
//Th = movemesh(Th,[x+ux*coef,y+uy*coef]);
//movescalar(vms);

// plot(vms,wait=1,value=1,fill=1,hsv=magma);

/* */

// vim: set ft=edp:

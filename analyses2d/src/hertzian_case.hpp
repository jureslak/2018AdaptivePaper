#ifndef HERTZ_SOLVER_HPP
#define HERTZ_SOLVER_HPP

#include <utility>
#include <array>
#include <Eigen/Dense>
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_fill_engines.hpp"
#include "io.hpp"
#include "mlsm_operators.hpp"
#include <Eigen/Sparse>

using namespace mm;
using namespace Eigen;
using namespace std;

const int TOP = -1;
const int RIGHT = -2;
const int BOTTOM = -3;
const int LEFT = -4;

template <typename domain_t, typename function_t>
void fill_domain_boundary(domain_t& domain, const function_t& f, double w) {
    Vec2d point(-w, 0);
    point[0] += f(point);
    while (point[0]+f(point) < w) {
        domain.addBoundaryPoint(point, TOP, Vec2d(0, 1));
        point[0] += f(point);
    }
    point = {w, 0};
    point[1] -= f(point);
    while (point[1]-f(point) > -w) {
        domain.addBoundaryPoint(point, RIGHT, Vec2d(1, 0));
        point[1] -= f(point);
    }
    point = {w, -w};
    point[0] -= f(point);
    while (point[0]-f(point) > -w) {
        domain.addBoundaryPoint(point, BOTTOM, Vec2d(0, -1));
        point[0] -= f(point);
    }
    point = {-w, -w};
    point[1] += f(point);
    while (point[1]+f(point) < 0) {
        domain.addBoundaryPoint(point, LEFT, Vec2d(-1, 0));
        point[1] += f(point);
    }
}

template <typename func_t>
RectangleDomain<Vec2d> make_domain(const XMLloader& conf, const func_t& dx) {
    double width = conf.get<double>("params.case.width");
    RectangleDomain<Vec2d> domain({-width, -width}, {width, 0});

    fill_domain_boundary(domain, dx, width);

    int seed = conf.get<int>("params.fill.seed");
    double pr = conf.get<double>("params.fill.pr");
    PoissonDiskSamplingFill fill; fill.proximity_relax(pr).randomize(true).seed(seed);
    domain.apply(fill, dx);

    return domain;
}

template <typename domain_t, typename func_t>
void refill_with_distr(const XMLloader& conf, domain_t& domain, const func_t& dx) {
    domain.clear();

    double width = conf.get<double>("params.case.width");
    fill_domain_boundary(domain, dx, width);

    int seed = conf.get<int>("params.fill.seed");
    double pr = conf.get<double>("params.fill.pr");
    PoissonDiskSamplingFill fill; fill.proximity_relax(pr).randomize(true).seed(seed);
    domain.apply(fill, dx);

    int riter = conf.get<int>("params.relax.iter");
    int num_neighbours = conf.get<int>("params.relax.num_neighbours");
    double init_heat = conf.get<double>("params.relax.init_heat");
    double final_heat = conf.get<double>("params.relax.final_heat");
    BasicRelax relax;
    relax.iterations(riter).projectionType(BasicRelax::DO_NOT_PROJECT).numNeighbours(num_neighbours)
            .initialHeat(init_heat).finalHeat(final_heat);
    domain.apply(relax, dx);
}

class Solver {
  public:

    /// Automatically precomputes some derived constant and other preprocessing.
    static void preprocess(XMLloader& conf);

    /**
     * Solve FWO case
     * @param conf Full problem configuration.
     */
    template<typename vec_t, template<class> class domain_t>
    static std::pair<Range<Vec<double, 2>>, Range<std::array<double, 3>>>
    solve(domain_t<vec_t>& domain, const XMLloader& conf, HDF5IO& out_file, Timer& timer);
};

void Solver::preprocess(XMLloader& conf) {
    double E = conf.get<double>("params.phy.E");
    double nu = conf.get<double>("params.phy.nu");
    double F = conf.get<double>("params.phy.F");
    double radius = conf.get<double>("params.case.radius");

    // parameter logic
    double mu = E / 2. / (1+nu);
    double lam = E * nu / (1-2*nu) / (1+nu);
    auto state = conf.get<std::string>("params.phy.state");
    assert_msg((state == "plane stress" || state == "plane strain"), "State is neither plane "
            "stress nor plane strain, but got '%s'.", state);
    if (state == "plane stress") {
        lam = 2 * mu * lam / (2 * mu + lam);  // plane stress
    }
    conf.set("params.phy.mu", mu);
    conf.set("params.phy.lam", lam);

    double Estar = E/(2*(1-nu*nu));
    double a = 2*std::sqrt(std::abs(F*radius/(M_PI*Estar)));
    double p0 = std::sqrt(std::abs(F*Estar/(M_PI*radius)));

    conf.set("params.phy.Estar", Estar);
    conf.set("params.phy.p0", p0);
    conf.set("params.case.a", a);
}

template <typename basis_t, typename domain_t>
std::pair<Range<Vec2d>, Range<array<double, 3>>> solve_(
        const XMLloader& conf, HDF5IO& file, Timer& timer, const domain_t& domain,  const basis_t& basis) {

    int N = domain.size();
    prn(N);

    const double lam = conf.get<double>("params.phy.lam");
    const double mu = conf.get<double>("params.phy.mu");
    const double a = conf.get<double>("params.case.a");
    const double force = conf.get<double>("params.phy.F");
    const double p0 = conf.get<double>("params.phy.p0");

    prn("shapes");
    timer.addCheckPoint("shapes");
    NNGaussians<Vec2d> weight(conf.get<double>("params.mls.sigmaW"));
    auto mls = make_mls(basis, weight);
    RaggedShapeStorage<domain_t, decltype(mls), mlsm::all> storage(domain, mls, domain.types != 0, false);
    MLSM<decltype(storage)> op(storage);  // All nodes, including boundary

    timer.addCheckPoint("matrix");
    prn("matrix");
    SparseMatrix<double, RowMajor> M(2*N, 2*N);
    Range<int> ss = storage.support_sizes();
    ss.append(ss); for (int& c : ss) { c *= 2; }
    M.reserve(ss);
    Eigen::VectorXd rhs = Eigen::VectorXd::Zero(2*N);

    for (int i : domain.types > 0) {
        op.graddiv(M, i, lam + mu);  // graddiv + laplace in interior
        op.lapvec(M, i,  mu);
        rhs(i) = 0;
        rhs(i+N) = 0;
    }

    for (int i : domain.types == TOP) {
        op.traction(M, i, lam, mu, {0, 1}, 1.0);
        double x = domain.positions[i][0];
        rhs(i) = 0;
        rhs(i+N) = (std::abs(x) < a) ? -p0 * std::sqrt(1 - x*x/a/a) : 0;
    }
    for (int i : domain.types == RIGHT) {
        op.valuevec(M, i, 1.0);
        rhs(i) = 0;
        rhs(i+N) = 0;
    }
    for (int i : domain.types == BOTTOM) {
        op.valuevec(M, i, 1.0);
        rhs(i) = 0;
        rhs(i+N) = 0;
    }
    for (int i : domain.types == LEFT) {
        op.valuevec(M, i, 1.0);
        rhs(i) = rhs(i+N) = 0;
    }

    VectorXd norms = (M.cwiseAbs()*VectorXd::Ones(M.cols())).cwiseInverse();
    M = norms.asDiagonal()*M;
    rhs = rhs.cwiseProduct(norms);

//    file.reopenFile();
//    file.reopenFolder();
//    file.setSparseMatrix("M", M);
//    file.setDoubleArray("rhs", rhs);
//    file.closeFile();

//    SparseMatrix<double> M2(M); M2.makeCompressed();

    timer.addCheckPoint("compute");

//          SparseLU<SparseMatrix<double>> solver;

    double droptol = conf.get<double>("params.solver.droptol");
    int fill_factor = conf.get<int>("params.solver.fill_factor");
    double errtol = conf.get<double>("params.solver.errtol");
    int maxiter = conf.get<int>("params.solver.maxiter");
    BiCGSTAB<SparseMatrix<double, RowMajor>, IncompleteLUT<double>> solver;
    solver.preconditioner().setDroptol(droptol);
    solver.preconditioner().setFillfactor(fill_factor);
    solver.setMaxIterations(maxiter);
    solver.setTolerance(errtol);

//        PardisoLU<SparseMatrix<double>> solver;
//    solver.compute(M2);
    prn("compute");
    solver.compute(M);
    prn("solve");
    timer.addCheckPoint("solve");
    VectorXd sol = solver.solve(rhs);

    prn(solver.iterations());
    prn(solver.error());

    timer.addCheckPoint("postprocess");

    Range<Vec2d> displ = reshape<2>(sol.head(2*N));
    Range<array<double, 3>> stress(N);
    for (int i = 0; i < domain.size(); ++i) {
        auto grad = op.grad(displ, i);
        stress[i][0] = (2*mu + lam)*grad(0,0) + lam*grad(1,1);  // sxx
        stress[i][1] = lam*grad(0,0) + (2*mu+lam)*grad(1,1);    // syy
        stress[i][2] = mu*(grad(0,1)+grad(1,0));                // sxy
    }
    return {displ, stress};
}

template<typename vec_t, template<class> class domain_t>

std::pair<Range<Vec2d>, Range<array<double, 3>>> Solver::solve(
        domain_t<vec_t>& domain, const XMLloader& conf, HDF5IO& file, Timer& timer) {
    std::vector<std::string> basis_names = {"mon9", "gau", "mq", "imq", "mon"};
    auto basis = conf.get<std::string>("params.mls.basis_type");
    assert_msg((std::find(basis_names.begin(), basis_names.end(), basis) != basis_names.end()),
               "Basis name '%s' not among valid basis names: %s.", basis, basis_names);

    double sigmaB = conf.get<double>("params.mls.sigmaB");
    int m = conf.get<int>("params.mls.m");

    if (basis == "mon9") {
        Monomials<Vec2d> mon9({{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}});
        return solve_(conf, file, timer, domain, mon9);
    } else if (basis == "gau") { return solve_(conf, file, timer, domain, NNGaussians<Vec2d>(sigmaB, m)); }
    else if (basis == "mq") { return solve_(conf, file, timer, domain, MultiQuadric<Vec2d>(sigmaB, m)); }
    else if (basis == "imq") { return solve_(conf, file, timer, domain, InverseMultiQuadric<Vec2d>(sigmaB, m)); }
    else if (basis == "mon") { return solve_(conf, file, timer, domain, Monomials<Vec2d>(m)); }
    throw std::runtime_error("Invalid basis.");
}

#endif // HERTZ_SOLVER_HPP

#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "draw.hpp"
#include "includes.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "types.hpp"
#include "io.hpp"
#include "adaptive_solver.hpp"
#include <Eigen/Sparse>
#include <Eigen/SparseLU>

using namespace std;
using namespace mm;
using namespace Eigen;

class Solver {
  public:
    template<typename vec_t, template<class> class domain_t>
    Eigen::VectorXd solve(domain_t<vec_t>& domain, const XMLloader& conf, HDF5IO& out_file, Timer& timer) {
        int basis_size = conf.get<int>("params.mls.m");
        double basis_sigma = conf.get<double>("params.mls.sigmaB");
        double weight_sigma = conf.get<double>("params.mls.sigmaW");

        string basis_type = conf.get<string>("params.mls.basis_type");
        string weight_type = conf.get<string>("params.mls.weight_type");

        if (basis_type == "gau") {
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, NNGaussians<vec_t>(basis_sigma, basis_size),
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, NNGaussians<vec_t>(basis_sigma, basis_size),
                              Monomials<vec_t>(1));
            }
        } else if (basis_type == "mon") {
            if (weight_type == "gau") {
                return solve_(domain, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              NNGaussians<vec_t>(weight_sigma));
            } else if (weight_type == "mon") {
                return solve_(domain, conf, out_file, timer, Monomials<vec_t>(basis_size),
                              Monomials<vec_t>(1));
            }
        }
        assert_msg(false, "Unknown basis type '%s' or weight type '%s'.", basis_type, basis_sigma);
    }


  private:
    template<typename domain_t, typename basis_t, typename weight_t>
    Eigen::VectorXd solve_(domain_t& domain, const XMLloader& conf, HDF5IO& out_file, Timer& timer,
                           const basis_t& basis, const weight_t& weight) {
        timer.addCheckPoint("start");
        int nx = conf.get<int>("params.num.nx");
        int N = domain.size();

        double alpha = conf.get<double>("params.case.alfa");
        auto mls = make_mls(basis, weight);
        RaggedShapeStorage<domain_t, decltype(mls), mlsm::lap> storage(domain, mls, domain.types != 0, false);
        SparseMatrix<double> M(N, N);
        M.reserve(storage.support_sizes());
        MLSM<decltype(storage)> op(storage);  // All nodes, including boundary
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);
        // Set equation on interior
        for (int i : (domain.types > 0)) {
            double x = domain.positions[i][0];
            op.lap(M, i, -1.0);  // laplace in interior
//            op.value(M, i, -std::pow(alpha+x, -4));
            rhs(i) = alpha*alpha*M_PI*M_PI*std::sin(alpha*x*M_PI);
//            rhs(i) = -2.0 *std::cos(1.0/(alpha+x))*std::pow(alpha+x, -3);
        }
        // Set boundary conditions
        for (int i : (domain.types < 0)) {
            double x = domain.positions[i][0];
            if (x == 0) {
                M.coeffRef(i, i) = 1;  // boundary conditions on the boundary
//                rhs(i) = std::sin(1.0/alpha);
                rhs(i) = 0;
            } else if (x == 1) {
                M.coeffRef(i, i) = 1;  // boundary conditions on the boundary
//                rhs(i) = std::sin(1.0/(alpha + x));
                rhs(i) = 0;
            } else {
                assert(!"Should not be here.");
            }
        }

        M.makeCompressed();
        SparseLU<SparseMatrix<double>> solver;

        double droptol = conf.get<double>("params.solver.droptol");
        int fill_factor = conf.get<int>("params.solver.fill_factor");
        double errtol = conf.get<double>("params.solver.errtol");
        int maxiter = conf.get<int>("params.solver.maxiter");
//        BiCGSTAB<SparseMatrix<double, RowMajor>, IncompleteLUT<double>> solver;
//        solver.preconditioner().setDroptol(droptol);
//        solver.preconditioner().setFillfactor(fill_factor);
//        solver.setMaxIterations(maxiter);
//        solver.setTolerance(errtol);

        out_file.setSparseMatrix("M", M);
        out_file.setDoubleArray("rhs", rhs);
        out_file.closeFolder();
        out_file.closeFile();
        solver.compute(M);
        out_file.openFile(out_file.getFileName());
        out_file.openFolder(out_file.getFolderName());

        return solver.solve(rhs);
    }
};

int main() {
    XMLloader conf("params/refine1d.xml");
    HDF5IO file("/mnt/data/ijs/meshless_analyses/refine/refine1d.h5", HDF5IO::DESTROY);

    int nx = conf.get<int>("params.num.nx");
    RectangleDomain<Vec1d> domain(0, 1);
    domain.fillUniformWithStep(1./nx, 1./nx);

    int num_links = conf.get<int>("params.ref.num_links");
    double fraction = conf.get<double>("params.ref.fraction");
//    domain.refine(domain.positions.filter([](const Vec1d& v) { return v[0] < 0.5;}), 3, 0.4, true);
//    domain.refine(domain.positions.filter([](const Vec1d& v) { return v[0] < 0.4;}), 3, 0.4, true);
//    domain.refine(domain.positions.filter([](const Vec1d& v) { return v[0] < 0.3;}), 3, 0.4, true);
//    domain.refine(domain.positions.filter([](const Vec1d& v) { return v[0] < 0.2;}), 3, 0.4, true);
//    domain.refine(domain.positions.filter([](const Vec1d& v) { return v[0] < 0.1;}), 3, 0.4, true);

    double alpha = conf.get<double>("params.case.alfa");
    file.openFolder("/");
    file.setDoubleAttribute("alpha", alpha);
    const std::function<double(Vec1d)> analytical = [=](const Vec1d& p) {
        double z = p[0];
//        return std::sin(1.0/(alpha + z));
        return std::sin(alpha*M_PI*z);
    };

    ToleranceCriterion criterion(conf.get<double>("params.est.estimator_tolerance"));
    Solver solver;

    string est_type = conf.get<string>("params.est.estimator_type");
    if (est_type == "dev") {
        DeviationEstimator estimator;
        solve_with_estimator(conf, domain, solver, estimator, criterion, file);
    } else if (est_type == "anal"){
        auto estimator = make_analytical_estimator(analytical);
        solve_with_estimator(conf, domain, solver, estimator, criterion, file);
    }
//    else if (est_type == "manual"){
//
//        solve_with_estimator(conf, domain, solver, estimator, criterion, file);
//        assert_msg(false, "Unknown type %s.", est_type);
//    }

    file.closeFile();

    return 0;
}

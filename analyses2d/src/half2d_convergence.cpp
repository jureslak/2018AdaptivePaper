#include "half2d_case.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_refine_engines.hpp"
#include "domain_support_engines.hpp"

using namespace mm;
using namespace Eigen;
using namespace std;

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XMLloader conf(argv[1]);

    Solver::preprocess(conf);

    std::string output_file = conf.get<std::string>("params.meta.file");
    HDF5IO file(output_file+"_wip.h5", HDF5IO::DESTROY);
    file.openFolder("/");
    file.setConf("conf", conf);
    file.closeFile();

    const double R = conf.get<double>("params.case.R");
    const double eps = conf.get<double>("params.case.eps");

    string nss = conf.get<string>("params.meta.ns");
    vector<string> ns = split(nss, " ");
    for (const string& s : ns) {
//        prn(s);
        int nx = stoi(s);

        Timer t;
        t.addCheckPoint("build");

        double dx = 1./nx;
        CircleDomain<Vec2d> domain = make_domain(conf, t, {0, 0}, R-eps, dx);

        int support_size = conf.get<int>("params.mls.n");
        FindBalancedSupport find_support(support_size, 2*support_size);
        domain.apply(find_support);

        t.addCheckPoint("write");

        file.reopenFile();
        string folder = format("/%06d", nx);
        prn(folder);
        file.openFolder(folder);
        file.setDomain("domain", domain);
        file.closeFile();

        Range<Vec2d> disp;
        Range<array<double, 3>> stress;
        Solver solver;
        std::tie(disp, stress) = solver.solve(domain, conf, file, t);

        t.addCheckPoint("end");

        file.reopenFile();
        file.reopenFolder();
        file.setFloat2DArray("disp", disp);
        file.setFloat2DArray("stress", stress);
        file.setTimer("time", t);
        file.closeFile();

        prn(t.getTime("build", "end"));
    }

    return 0;
}

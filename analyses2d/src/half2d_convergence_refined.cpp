#include "half2d_case.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_refine_engines.hpp"
#include "domain_support_engines.hpp"

using namespace mm;
using namespace Eigen;
using namespace std;

int main() {
    XMLloader conf("params/half2d_convergence_refined.xml");
    std::string output_file = conf.get<std::string>("params.meta.file");
    HDF5IO file("/mnt/data/ijs/meshless_analyses/refine/"+output_file+"_wip.h5", HDF5IO::DESTROY);
    file.openFolder("/");
    file.setConf("conf", conf);

    const double R = conf.get<double>("params.case.R");
    const double eps = conf.get<double>("params.case.eps");
    const double E = conf.get<double>("params.case.E");
    const double v = conf.get<double>("params.case.nu");
    // Derived parameters
    double lam = E * v / (1 - 2 * v) / (1 + v);
    const double mu = E / 2 / (1 + v);
    lam = 2*mu*lam / (2*mu + lam);  // plane stress
    file.openFolder("/conf");
    file.setDoubleAttribute("lam", lam);
    file.setDoubleAttribute("mu", mu);
    file.closeFile();

    int nx = conf.get<int>("params.num.nx");

    string nss = conf.get<string>("params.meta.ns");
    vector<string> nrefs = split(nss, " ");
    for (int refine_level = 0; refine_level < nrefs.size(); ++refine_level) {
        prn(refine_level);
        int nref = stoi(nrefs[refine_level]);

        Timer t;
        t.addCheckPoint("build");

        double dx = 1./nx;
        CircleDomain<Vec2d> domain = make_domain(conf, t, {0, 0}, R-eps, dx);
        int riter = conf.get<int>("params.num.riter");
        int num_neighbours = conf.get<int>("params.num.num_neighbours");
        double init_heat = conf.get<double>("params.num.init_heat");
        double final_heat = conf.get<double>("params.num.final_heat");
        BasicRelax relax;
        relax.iterations(riter).projectionType(2).numNeighbours(num_neighbours)
                .initialHeat(init_heat).finalHeat(final_heat);

        int support_size = conf.get<int>("params.mls.n");
        FindBalancedSupport find_support(support_size, 2*support_size);

        // refine
        HalfLinksRefine refine;
        for (int i = 0; i < nref; ++i) {
//            RelaxFunction<Vec2d> relax_function(domain.positions, 2);
            domain.apply(relax);
            domain.apply(find_support);
            Range<int> idx = domain.positions.filter([=] (const Vec2d& v) {
                return (Vec2d(0, R-eps) - v).norm() < (R-eps)*((double) i+1)/(nref+1);
            });
            refine.region(idx);
            domain.apply(refine);
        }

        Range<int> left = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[0]) < 1e-6; });
        domain.types[left] = -3;
        Range<int> bottom = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[1]) < 1e-6; });
        domain.types[bottom] = -2;
        Range<int> center = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[1]) + std::abs(v[0]) < 1e-6; });
        domain.types[center] = -4;

        RelaxFunction<Vec2d> relax_function(domain.positions, 2);
        domain.apply(relax, relax_function);

        domain.apply(find_support);

        t.addCheckPoint("write");

        file.reopenFile();
        file.openFolder(format("/%02d", nref));
        file.setDomain("domain", domain);
        file.closeFile();

        Range<Vec2d> disp;
        Range<array<double, 3>> stress;
        Solver solver;
        std::tie(disp, stress) = solver.solve(domain, conf, file, t);
        t.addCheckPoint("save");

        file.reopenFile();
        file.reopenFolder();
        file.setFloat2DArray("disp", disp);
        file.setFloat2DArray("stress", stress);

        t.addCheckPoint("end");

        file.setTimer("time", t);

        prn(t.getTime("build", "end"));
    }

    return 0;
}

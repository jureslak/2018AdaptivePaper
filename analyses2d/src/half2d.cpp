#include "half2d_case.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_refine_engines.hpp"

using namespace mm;
using namespace Eigen;
using namespace std;

int main() {
    XMLloader conf("params/half2d.xml");
    std::string output_file = conf.get<std::string>("params.meta.file");
    HDF5IO file("/mnt/data/ijs/meshless_analyses/refine/"+output_file+"_wip.h5", HDF5IO::DESTROY);

    Timer t;
    t.addCheckPoint("build");

    const int nx = conf.get<int>("params.num.nx");
    const double R = conf.get<double>("params.case.R");
    const double eps = conf.get<double>("params.case.eps");

    double dx = 2./nx;
    CircleDomain<Vec2d> domain = make_domain(conf, t, {0, 0}, R-eps, dx);
    int riter = conf.get<int>("params.num.riter");
    DiffuseAnnealingRelax relax; relax.iterations(riter);
    int support_size = conf.get<int>("params.mls.n");
    domain.findSupport(support_size);

    t.addCheckPoint("write");

    const double P = conf.get<int>("params.case.P");

    file.openFolder("/");
    file.setDoubleAttribute("P", P);
    file.setDoubleAttribute("R", R);
    file.setDoubleAttribute("eps", eps);
    file.setDomain("domain", domain);
    file.closeFile();

    Range<Vec2d> disp;
    Range<array<double, 3>> stress;
    Solver solver;
    t.addCheckPoint("solve");
    std::tie(disp, stress) = solver.solve(domain, conf, file, t);
    t.addCheckPoint("save");

    file.reopenFile();
    file.reopenFolder();
    file.setFloat2DArray("disp", disp);
    file.setFloat2DArray("stress", stress);
    file.closeFile();

    t.addCheckPoint("end");
    t.showTimings();

    return 0;
}

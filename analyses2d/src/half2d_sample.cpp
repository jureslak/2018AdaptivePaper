#include "half2d_case.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_refine_engines.hpp"
#include "domain_support_engines.hpp"

using namespace mm;
using namespace Eigen;
using namespace std;

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XMLloader conf(argv[1]);

    Solver::preprocess(conf);

    std::string output_file = conf.get<std::string>("params.meta.file");
    HDF5IO file(output_file+"_wip.h5", HDF5IO::DESTROY);
    file.openFolder("/");
    file.setConf("conf", conf);
    file.closeFile();

    Timer t;
    t.addCheckPoint("build");

    std::string domain_filename = conf.get<std::string>("params.meta.domain_file");
    HDF5IO domain_file(domain_filename, HDF5IO::READONLY);
    LoadedDomain<Vec2d> domain = LoadedDomain<Vec2d>::load(domain_file, "domain");
    Range<int> left = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[0]) < 1e-6; });
    domain.types[left] = -3;
    Range<int> bottom = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[1]) < 1e-6; });
    domain.types[bottom] = -2;
    Range<int> center = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[1]) + std::abs(v[0]) < 1e-6; });
    domain.types[center] = -4;

    int N = domain.size();
    prn(N);

    prn("finding support");
    int support_size = conf.get<int>("params.mls.n");
    FindBalancedSupport find_support(support_size, support_size+5);
    domain.apply(find_support);

    t.addCheckPoint("write");

    file.reopenFile();
    file.openFolder("/");
    file.setDomain("domain", domain);
    file.closeFile();

    prn("shapes");
    Range<Vec2d> disp;
    Range<array<double, 3>> stress;
    Solver solver;
    std::tie(disp, stress) = solver.solve(domain, conf, file, t);
    t.addCheckPoint("save");

    file.reopenFile();
    file.reopenFolder();
    file.setFloat2DArray("disp", disp);
    file.setFloat2DArray("stress", stress);

    t.addCheckPoint("end");

    file.setTimer("time", t);

    prn(t.getTime("build", "end"));

    return 0;
}

#include "half2d_case.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_refine_engines.hpp"
#include "domain_support_engines.hpp"
#include "domain_fill_engines.hpp"

using namespace mm;
using namespace Eigen;
using namespace std;

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XMLloader conf(argv[1]);

    Solver::preprocess(conf);

    std::string output_file = conf.get<std::string>("params.meta.file");
    HDF5IO file(output_file+"_wip.h5", HDF5IO::DESTROY);
    file.openFolder("/");
    file.setConf("conf", conf);
    file.closeFile();

    const double R = conf.get<double>("params.case.R");
    const double eps = conf.get<double>("params.case.eps");

    string dxss = conf.get<string>("params.meta.dxs");
    vector<string> dxs = split(dxss, ",");
    for (const string& s : dxs) {
        prn(s);
        double dx = stod(s);

        Timer t;
        t.addCheckPoint("build");

        // domain
        CircleDomain<Vec2d> domain({0, 0}, R-eps);
        double cutR = (R-eps)*1.1;
        RectangleDomain<Vec2d> cutout({-cutR, -cutR}, {cutR, 0});
        RectangleDomain<Vec2d> cutout2({-cutR, -cutR}, {0, cutR});
        domain.subtract(cutout);
        domain.subtract(cutout2);

        // fill
        double alpha = conf.get<double>("params.num.alpha");
        double max_density_factor = conf.get<double>("params.num.max_density_factor");
        auto fill_density = [=](const Vec2d& p) {
            double dist = (p-Vec2d(0, R-eps)).norm()/(R-eps)/std::sqrt(2);  // in [0, 1]
            return std::pow(dist, alpha)*(1 - 1.0/max_density_factor)*dx + dx/max_density_factor;
        };
        fill_domain_boundary(domain, fill_density, R-eps);

        prn(domain.size());

        int seed = conf.get<int>("params.num.seed");
        PoissonDiskSamplingFill fill;
        fill.proximity_relax(0.9).randomize(true).seed(seed);

        domain.apply(fill, fill_density);
        int N = domain.size();
        prn(N);

        int riter = conf.get<int>("params.num.riter");
        int num_neighbours = conf.get<int>("params.num.num_neighbours");
        double init_heat = conf.get<double>("params.num.init_heat");
        double final_heat = conf.get<double>("params.num.final_heat");
        BasicRelax relax;
        relax.iterations(riter).numNeighbours(num_neighbours).initialHeat(init_heat)
                .finalHeat(final_heat).projectionType(BasicRelax::DO_NOT_PROJECT);
        domain.apply(relax, fill_density);

        int support_size = conf.get<int>("params.mls.n");
        FindBalancedSupport find_support(support_size, 2*support_size);
        domain.apply(find_support);
        prn("support found");

        t.addCheckPoint("write");

        file.reopenFile();
        file.openFolder(format("/%.6f", dx));
        file.setDomain("domain", domain);
        file.closeFile();
        prn(file.getFolderName());

        prn("solve");
        Range<Vec2d> disp;
        Range<array<double, 3>> stress;
        Solver solver;
        std::tie(disp, stress) = solver.solve(domain, conf, file, t);
        t.addCheckPoint("save");

        file.reopenFile();
        file.reopenFolder();
        file.setFloat2DArray("disp", disp);
        file.setFloat2DArray("stress", stress);

        t.addCheckPoint("end");

        file.setTimer("time", t);

        prn(t.getTime("build", "end"));
    }

    return 0;
}

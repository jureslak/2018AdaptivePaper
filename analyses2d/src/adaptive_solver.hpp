#include <iostream>
#include <vector>
#include "common.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_refine_engines.hpp"
#include "domain_relax_engines.hpp"
#include "draw.hpp"
#include "mls.hpp"
#include "mlsm_operators.hpp"
#include "io.hpp"
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseLU>

using namespace std;
using namespace mm;
using namespace Eigen;

class ToleranceCriterion {
    double tolerance;
  public:
    explicit ToleranceCriterion(double tolerance_) : tolerance(tolerance_) {
        assert_msg(0 <= tolerance && tolerance <= 1, "Tolerance should be in [0, 1], got %f.",
                   tolerance);
    }
    Range<int> getBadNodes(const VectorXd& error) {
        double max_error = error.maxCoeff();
        Range<int> result;
        for (int i = 0; i < error.size(); ++i) {
            if (error(i) >= tolerance * max_error) {
                result.push_back(i);
            }
        }
        return result;
    }
};

void save_solution(HDF5IO& out_file, const VectorXd& solution) {
    out_file.reopenFile();
    out_file.reopenFolder();
    out_file.setDoubleArray("sol", solution);
    out_file.closeFile();
}
void save_solution(HDF5IO& out_file, const Range<array<double, 3>>& solution) {
    out_file.reopenFile();
    out_file.reopenFolder();
    out_file.setDouble2DArray("sol", solution);
    out_file.closeFile();
}
void save_solution(HDF5IO& out_file, const std::pair<Range<Vec2d>, Range<array<double, 3>>>& solution) {
    out_file.reopenFile();
    out_file.reopenFolder();
    out_file.setDouble2DArray("stress", solution.second);
    out_file.setDouble2DArray("displ", solution.first);
    out_file.closeFile();
}

template <typename domain_t, typename Estimator, typename Criterion, typename Solver>
void solve_with_estimator(const XMLloader& conf, domain_t& domain, Solver& solver, Estimator& e,
                          Criterion& c, HDF5IO& out_file) {
    Timer t;
    t.addCheckPoint("start");

    int N = domain.size();
    int support_size = conf.get<int>("params.mls.n");
    FindBalancedSupport find_support(support_size, 2*support_size);
    domain.apply(find_support);

    int iter = 0;
    out_file.reopenFile();
    out_file.openFolder(format("/iter%04d", iter));
    out_file.setIntAttribute("N", N);
    out_file.setDomain("domain", domain);
    out_file.closeFile();

    auto solution = solver.solve(domain, conf, out_file, t);
    save_solution(out_file, solution);

    VectorXd error = e.estimate(domain, solution);
    out_file.reopenFile();
    out_file.reopenFolder();
    out_file.setDoubleArray("error_indicator", error);
    out_file.closeFile();
    prn(error.maxCoeff());

    double global_error_tolerance = conf.get<double>("params.est.global_error_tolerance");
    int total_max_iter = conf.get<int>("params.est.total_max_iter");
    int riter = conf.get<int>("params.num.iter");
    int num_neighbours = conf.get<int>("params.num.num_neighbours");
    double init_heat = conf.get<double>("params.num.init_heat");
    double final_heat = conf.get<double>("params.num.final_heat");
    BasicRelax relax;
    relax.iterations(riter).projectionType(BasicRelax::DO_NOT_PROJECT).numNeighbours(num_neighbours)
         .initialHeat(init_heat).finalHeat(final_heat);

    double imb = conf.get<double>("params.num.imbalance");
    double min_dist = conf.get<double>("params.num.min_dist");
    while (error.maxCoeff() >= global_error_tolerance && iter < total_max_iter) {
        iter++;
        t.clear();
        prn(iter);

        Range<int> to_refine = c.getBadNodes(error);
        HalfLinksRefine refine; refine.region(to_refine);
//        HalfLinksSmoothRefine refine; refine.max_imbalance(imb).min_dist(min_dist).region(to_refine);
//        domain.apply(refine);
        RelaxFunction<Vec2d> relaxFunction(domain.positions, 2);
        domain.apply(relax, relaxFunction);
//        domain.apply(relax);
        Range<int> left = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[0]) < 1e-6; });
        domain.types[left] = -3;
        Range<int> bottom = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[1]) < 1e-6; });
        domain.types[bottom] = -2;
        Range<int> center = domain.positions.filter([] (const Vec2d& v) { return std::abs(v[1]) + std::abs(v[0]) < 1e-6; });
        domain.types[center] = -4;

        out_file.reopenFile();
        out_file.openFolder(format("/iter%04d", iter));
        N = domain.size();
        out_file.setIntAttribute("N", N);
        domain.apply(find_support);
        //        domain.symmetrizeSupport();
        out_file.setDomain("domain", domain);

        solution = solver.solve(domain, conf, out_file, t);
        save_solution(out_file, solution);

        error = e.estimate(domain, solution);
        out_file.reopenFile();
        out_file.reopenFolder();
        out_file.setDoubleArray("error_indicator", error);
        out_file.closeFile();

        prn(error.maxCoeff());

    }
    prn(error.maxCoeff());
    if (iter == total_max_iter) {
        cout << "Stopped after " << iter << " iterations." << endl;
    } else {
        cout << "Converged after " << iter << " iterations." << endl;
    }
    out_file.closeFolder();
}

template <typename domain_t, typename Estimator, typename Solver, typename Func>
void solve_with_estimator_move(const XMLloader& conf, domain_t& domain, Solver& solver, Estimator& e,
                               HDF5IO& out_file, const Func& largest_allowed, std::string folder_name="") {
    Timer t;
    t.addCheckPoint("start");

    auto pre_relax_domain = domain.makeClone(domain);

    int riter = conf.get<int>("params.num.riter");
    int num_neighbours = conf.get<int>("params.num.num_neighbours");
    double init_heat = conf.get<double>("params.num.init_heat");
    double final_heat = conf.get<double>("params.num.final_heat");
    BasicRelax relax;
    relax.iterations(riter).projectionType(BasicRelax::DO_NOT_PROJECT).numNeighbours(num_neighbours)
            .initialHeat(init_heat).finalHeat(final_heat);
//    domain.apply(relax, dx);

    int N = domain.size();
    int support_size = conf.get<int>("params.mls.n");
    FindBalancedSupport find_support(support_size, 2*support_size);
    domain.apply(find_support);

    int iter = 0;
    out_file.reopenFile();
    out_file.openFolder(format("/iter%04d", iter));
    out_file.setIntAttribute("N", N);
    out_file.setDomain("domain", domain);
    out_file.closeFile();

    auto solution = solver.solve(domain, conf, out_file, t);
    save_solution(out_file, solution);

    VectorXd error = e.estimate(domain, solution);
    out_file.reopenFile();
    out_file.reopenFolder();
    out_file.setDoubleArray("error_indicator", error);
    out_file.closeFile();
    prn(error.maxCoeff());

    double global_error_tolerance = conf.get<double>("params.est.global_error_tolerance");
    double err_threshold = global_error_tolerance;
    double err_threshold_lower = conf.get<double>("params.est.derefine_error_tolerance");
    double aggressiveness = conf.get<double>("params.est.aggressiveness");
    double aggressiveness_deref = conf.get<double>("params.est.aggressiveness_deref");

    int total_max_iter = conf.get<int>("params.est.total_max_iter");

    while (error.sum()/N >= global_error_tolerance && iter < total_max_iter) {
        iter++;
        t.clear();
        prn(iter);

        double min_e = error.minCoeff();
        double max_e = error.maxCoeff();
        prn(min_e);
        prn(max_e);
        Range<double> new_dx(N);
        prn(err_threshold);
        pre_relax_domain.findSupport(2);
        int ref = 0, deref = 0, same = 0, limit = 0;
        for (int i = 0; i < N; ++i) {
            double old_dx = std::sqrt(pre_relax_domain.distances[i][1]);
            if (error(i) >= err_threshold) {
                double badness = (error(i) - err_threshold) / (max_e - err_threshold);  // in [0, 1]
                new_dx[i] = old_dx / (badness * (aggressiveness - 1) + 1);
                ref++;
            } else if (error(i) <= err_threshold_lower) {
                double badness = (err_threshold_lower - error(i)) / (err_threshold_lower - min_e);
                new_dx[i] = old_dx / (1+badness*(1.0/aggressiveness_deref - 1));
                if (new_dx[i] < old_dx) { prn("Derefine is wrong."); exit(1); }
                double ll = largest_allowed(domain.positions[i]);
                if (new_dx[i] > ll) {
//                    std::cerr << "proposed new dx was " << new_dx[i] << " which was too large, so I reduced it to " << ll << std::endl;
                    new_dx[i] = ll;  limit++; }
                else { deref++; }
            } else {
                new_dx[i] = old_dx;
                same++;
            }
        }
        std::cerr << ref << " nodes refined, "
                  << deref << " derefined, "
                  << limit << " hit derefine limit, "
                  << same << " left same.\n";

        out_file.reopenFile();
        out_file.reopenFolder();
        out_file.setDoubleArray("dx", new_dx);
        out_file.closeFile();

        int dx_num_closest = conf.get<double>("params.est.scattered_interpolant_neighbours");
        ModifiedSheppardsScatteredInterpolant<Vec2d, double> new_distr(domain.positions, new_dx, dx_num_closest);
//        ScatteredInterpolant<Vec2d, double> new_distr(domain.positions, new_dx, dx_num_closest);

        refill_with_distr(conf, domain, new_distr);
        pre_relax_domain = pre_relax_domain.makeClone(domain);

        domain.apply(relax, new_distr);

//        prn(domain);

        N = domain.size();
//        if (N > 1e5) {
//            out_file.reopenFile();
//            out_file.reopenFolder();
//            out_file.setIntAttribute("converged",  0);
//            out_file.closeFile();
//            break;
//        }

        out_file.reopenFile();
        out_file.openFolder(format("%s/iter%04d", folder_name, iter));
        out_file.setIntAttribute("N", N);
        domain.apply(find_support);
        out_file.setDomain("domain", domain);

        solution = solver.solve(domain, conf, out_file, t);
        save_solution(out_file, solution);

        error = e.estimate(domain, solution);
        out_file.reopenFile();
        out_file.reopenFolder();
        out_file.setDoubleArray("error_indicator", error);
        out_file.closeFile();
//
        prn(error.maxCoeff());

    }
    prn(error.maxCoeff());
    if (iter == total_max_iter) {
        cout << "Stopped after " << iter << " iterations." << endl;
    } else {
        cout << "Converged after " << iter << " iterations." << endl;
    }

}

#include "half2d_case.hpp"
#include "domain.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"
#include "domain_refine_engines.hpp"
#include "domain_support_engines.hpp"
#include "adaptive_solver.hpp"

using namespace mm;
using namespace Eigen;
using namespace std;

class SolverStress {
  public:
    template <typename domain_t>
    Range<array<double, 3>> solve(domain_t& domain, const XMLloader& conf, HDF5IO& out_file, Timer& timer) {
        Solver solver;
        std::pair<Range<Vec2d>, Range<array<double, 3>>> res = solver.solve(domain, conf, out_file, timer);
        return res.second;
    }
};

template<typename Callable>
class EnergyDensityEstimator {
    Callable analytical;
    double lam, mu;

  public:
    explicit EnergyDensityEstimator(Callable analytical_, double lam, double mu) :
            analytical(analytical_), lam(lam), mu(mu) {}

    /**
     * Returns an estimate of error for each domain node.
     * @param domain The discretization of the domain.
     * @param solution Approximated solution in domain nodes.
     * @return An approximation of error in domain nodes.
     */
    template<typename domain_t>
    VectorXd estimate(const domain_t& domain, const Range<std::array<double, 3>>& stress) {
        VectorXd error = VectorXd::Zero(stress.size());
        for (int i = 0; i < domain.size(); ++i) {
            Eigen::Matrix2d m = analytical(domain.positions[i]);
            error(i) = energy_density_kernel(stress[i][0] - m(0, 0), stress[i][1] - m(1, 1),
                                             stress[i][2] - m(0, 1), lam, mu);
            error(i) *= domain.distances[i][1];
        }
        return error;
    }
};

int main() {
    XMLloader conf("params/half2d_convergence_indicator.xml");
    std::string output_file = conf.get<std::string>("params.meta.file");
    HDF5IO file("/mnt/data/ijs/meshless_analyses/refine/"+output_file+"_wip.h5", HDF5IO::DESTROY);
    file.openFolder("/");
    file.setConf("conf", conf);

    const double R = conf.get<double>("params.case.R");
    const double eps = conf.get<double>("params.case.eps");
    const double E = conf.get<double>("params.case.E");
    const double v = conf.get<double>("params.case.nu");
    const double P = conf.get<double>("params.case.P");

    // Derived parameters
    double lam = E * v / (1 - 2 * v) / (1 + v);
    const double mu = E / 2 / (1 + v);
    lam = 2*mu*lam / (2*mu + lam);  // plane stress
    file.openFolder("/conf");
    file.setDoubleAttribute("lam", lam);
    file.setDoubleAttribute("mu", mu);
    file.closeFile();

    int nx = conf.get<int>("params.num.nx");

    ToleranceCriterion criterion(conf.get<double>("params.est.estimator_tolerance"));
    SolverStress solver;

    Timer t;
    t.addCheckPoint("begin");

    CircleDomain<Vec2d> domain = make_domain(conf, t, {0, 0}, R-eps, (R-eps)/nx);

    string est_type = conf.get<string>("params.est.estimator_type");
//    if (est_type == "dev") {
//        DeviationEstimator estimator;
//        solve_with_estimator(conf, domain, solver, estimator, criterion, file);
//    } else if (est_type == "anal"){
        auto anal_sol = [=](const Vec2d& p) { return analytical(p, P, R); };
        EnergyDensityEstimator<decltype(anal_sol)> estimator(anal_sol, lam, mu);
        solve_with_estimator(conf, domain, solver, estimator, criterion, file);
//    }

    file.closeFile();

    t.addCheckPoint("end");
    prn(t.getTime("begin", "end"));

    return 0;
}

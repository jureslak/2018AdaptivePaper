#ifndef COMPRESSED_DISK_ESTIMATORS_HPP
#define COMPRESSED_DISK_ESTIMATORS_HPP


template<typename Callable>
class AnalyticalEstimator {
    Callable analytical;

  public:
    explicit AnalyticalEstimator(Callable analytical_) : analytical(analytical_) {}

    /**
     * Returns an estimate of error for each domain node.
     * @param domain The discretization of the domain.
     * @param solution Approximated solution in domain nodes.
     * @return An approximation of error in domain nodes.
     */
    template<typename domain_t>
    VectorXd estimate(const domain_t& domain, const VectorXd& solution) {
        VectorXd error = VectorXd::Zero(solution.size());
        for (int i = 0; i < domain.size(); ++i) {
            error(i) = std::abs(analytical(domain.positions[i]) - solution(i));
        }
        return error;
    }
};

template<typename Callable>
static AnalyticalEstimator<Callable> make_analytical_estimator(Callable callable) {
    return AnalyticalEstimator<Callable>(callable);
}

class DeviationEstimator {
    double get_deviation(const Range<int>& ind, const VectorXd& solution) {
        double mu = 0;
        for (int i : ind) {
            mu += solution[i];
        }
        mu /= ind.size();

        double var = 0;
        for (int i : ind) {
            var += (solution[i] - mu)*(solution[i] - mu);
        }
        return std::sqrt(var);
    }
  public:
    /**
     * Returns an estimate of error for each domain node.
     * @param domain The discretization of the domain.
     * @param solution Approximated solution in domain nodes.
     * @return An approximation of error in domain nodes.
     */
    template<typename domain_t>
    VectorXd estimate(const domain_t& domain, const VectorXd& solution) {
        VectorXd error = VectorXd::Zero(solution.size());
        for (int i = 0; i < domain.size(); ++i) {
            error(i) = get_deviation(domain.support[i], solution);
        }
        return error;
    }

    /**
     * Returns an estimate of error for each domain node.
     * @param domain The discretization of the domain.
     * @param solution Approximated solution in domain nodes.
     * @return An approximation of error in domain nodes.
     */
    template<typename domain_t>
    VectorXd estimate(const domain_t& domain, const std::pair<Range<Vec2d>, Range<std::array<double, 3>>>& sol) {
        auto stress = sol.second;
        VectorXd error = VectorXd::Zero(stress.size());

        VectorXd sxx = VectorXd::Zero(stress.size()), sxy = VectorXd::Zero(stress.size()), syy = VectorXd::Zero(stress.size());
        for (int i = 0; i < domain.size(); ++i) {
            sxx[i] = stress[i][0];
            syy[i] = stress[i][1];
            sxy[i] = stress[i][2];
        }

        for (int i = 0; i < domain.size(); ++i) {
            error(i) = get_deviation(domain.support[i], sxx) + get_deviation(domain.support[i], sxy) + get_deviation(domain.support[i], syy);
        }
        return error;
    }
};

/*
template<typename Callable>
class EnergyDensityEstimator {
    Callable analytical;
    double lam, mu;

  public:
    EnergyDensityEstimator(Callable analytical_, double lam, double mu) :
            analytical(analytical_), lam(lam), mu(mu) {}

    /**
     * Returns an estimate of error for each domain node.
     * @param domain The discretization of the domain.
     * @param solution Approximated solution in domain nodes.
     * @return An approximation of error in domain nodes.
     /
    template<typename domain_t>
    VectorXd estimate(const domain_t& domain, const Range<std::array<double, 3>>& stress) {
        VectorXd error = VectorXd::Zero(stress.size());
        for (int i = 0; i < domain.size(); ++i) {
            Eigen::Matrix2d m = analytical(domain.positions[i]);
            error(i) = energy_density_kernel(stress[i][0] - m(0, 0), stress[i][1] - m(1, 1),
                                             stress[i][2] - m(0, 1), lam, mu);
            error(i) *= domain.distances[i][1];
        }
        return error;
    }
};

*/

template<typename Callable>
class L1Estimator {
    Callable analytical;

  public:
    explicit L1Estimator(Callable analytical_) : analytical(analytical_) {}

    template<typename domain_t>
    VectorXd estimate(const domain_t& domain, const Range<std::array<double, 3>>& stress) {
        VectorXd error = VectorXd::Zero(stress.size());
        for (int i = 0; i < domain.size(); ++i) {
            Eigen::Matrix2d m = analytical(domain.positions[i]);
            error(i) = std::abs(stress[i][0] - m(0, 0)) + std::abs(stress[i][1] - m(1, 1))
                       + std::abs(stress[i][2] - m(0, 1));
            error(i) *= domain.distances[i][1];
        }
        return error;
    }
};


template<typename Callable>
class LinfEstimator {
    Callable analytical;

  public:
    explicit LinfEstimator(Callable analytical_) : analytical(analytical_) {}

    template<typename domain_t>
    VectorXd estimate(const domain_t& domain, const Range<std::array<double, 3>>& stress) {
        VectorXd error = VectorXd::Zero(stress.size());
        for (int i = 0; i < domain.size(); ++i) {
            Eigen::Matrix2d m = analytical(domain.positions[i]);
            error(i) = std::max(std::abs(m(0, 0) - stress[i][0]),
                                std::max(std::abs(m(1, 1) - stress[i][1]),
                                         std::abs(m(0, 1) - stress[i][2])));
        }
        return error;
    }
};

#endif //COMPRESSED_DISK_ESTIMATORS_HPP

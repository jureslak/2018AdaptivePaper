prepare

casename = 'half2d_convergence_moving_alpha_analysis';

load([plotdatapath casename '.mat'])

f1 = setfig('b1', [500 500]);
plot(data(:, 1), data(:, 2), 'o-')
% legend('$$\|\cdot\|_1$', '$\|\cdot\|_\infty$', '$\|\cdot\|_E$');
set(gca, 'xscale', 'log')
% set(gca, 'yscale', 'log')
xlabel('$\alpha$')
labels = cell(size(data(1:2:end, 1)));
c = 1;
for i = data(1:2:end, 1)'
    labels{c} = sprintf('%g', i);
    c = c + 1;
end
labels{2} = '';
labels{end} = '$\ \ $100';
xticks(data(1:2:end, 1))
xticklabels(labels)
ylabel('iterations until convergence')
% title('Error')
% exportf(f1, [imagepath 'alpha_analysis_iter.pdf']);

f2 = setfig('b2', [500 500]);
plot(data(:, 1), data(:, 3), 'o-')
% legend('$$\|\cdot\|_1$', '$\|\cdot\|_\infty$', '$\|\cdot\|_E$');
set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
xticks(data(1:2:end, 1))
xticklabels(labels)
xlabel('$\alpha$')
ylabel('number of nodes in final iteration')
% exportf(f2, [imagepath 'alpha_analysis_nodes.pdf']);

f3 = setfig('b3', [500 500]);
plot(data(:, 1), data(:, 5), 'o-')
plot(data(:, 1), data(:, 4), 'o-')
plot(data(:, 1), data(:, 6), 'o-')
legend('$e_\infty$', '$e_1$', '$e_E$');
set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
xticks(data(1:2:end, 1))
xticklabels(labels)
xlabel('$\alpha$')
ylabel('Error in final iteration')
% exportf(f3, [imagepath 'alpha_analysis_error.pdf']);
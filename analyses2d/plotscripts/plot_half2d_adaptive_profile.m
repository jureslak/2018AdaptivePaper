prepare

l = [-0.1, -1, -80];
u = [0.5, 11, 70];

i = 1;
for c = [0.2, 0.02, 0.002]
    
s = sprintf('%g', c);
casename = ['half2d_convergence_moving_' s(3:end) '_v3_profiles'];
load([plotdatapath casename '.mat'])

f1 = setfig(['bo' sprintf('%d', i)], [400, 500]);

plot(profiles.y, profiles.asxy, 'r-', 'LineWidth', 2);
leg = {'analytical'};
cmap = repmat(linspace(0.7, 0, length(profiles.iter))', 1, 3);
for j = 1:length(profiles.iter)
    plot(profiles.y, profiles.iter{j}.sxy, '-', 'Color', cmap(j, :));
    leg{end+1} = sprintf('iter %d', j-1);
end
legend(leg, 'Location', 'NW');
ylim([l(i), u(i)])
xlabel('$y$, $x = R - \gamma - y$')
ylabel('$\sigma_{xy}$')
% xlim([-inf, inf])
title(sprintf('$\\gamma = %g$', eps));

% exportf(f1, [imagepath casename '.pdf'])

zoomin = [
    0.1960    0.2360    0.4482    0.4682;
    0.4565    0.4770    5.0637    6.1817;
    0.4956    0.4975   51.1281   61.0217];

f2 = setfig(['bo' sprintf('%d', i+12)], [400, 500]);

plot(profiles.y, profiles.asxy, 'r-', 'LineWidth', 2);
leg = {'analytical'};
cmap = repmat(linspace(0.7, 0, length(profiles.iter))', 1, 3);
for j = 1:length(profiles.iter)
    plot(profiles.y, profiles.iter{j}.sxy, '-', 'Color', cmap(j, :));
    leg{end+1} = sprintf('iter %d', j-1);
end
legend(leg, 'Location', 'NW');
xlim([zoomin(i, 1), zoomin(i, 2)]);
ylim([zoomin(i, 3), zoomin(i, 4)])
xlabel('$y$, $x = R - \gamma - y$')
ylabel('$\sigma_{xy}$')
% xlim([-inf, inf])
title(sprintf('$\\gamma = %g$', eps));

% exportf(f2, [imagepath casename '_zoom.pdf'])

i = i + 1;
end

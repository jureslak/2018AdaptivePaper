prepare

i = 1;
for c = [0.2, 0.02, 0.002]
    
s = sprintf('%g', c);
casename = ['half2d_convergence_moving_' s(3:end) '_v3'];
load([plotdatapath casename '.mat'])

f1 = setfig(['bo' sprintf('%d', i)], [400, 500]);
ms = 5.5;
plot(data(:, 1), data(:, 5), 'x-', 'MarkerSize', ms)
% plot(data(:, 1), data(:, 4), 'o-')
plot(data(:, 1), data(:, 3), 'o-', 'MarkerSize', ms)
plot(data(:, 1), data(:, 6), '<-', 'MarkerSize', ms)
set(gca, 'YScale', 'log');
ylabel('error')
ylim([1e-4 1e2])
xlim([-inf, inf])
xlabel('iteration')
yyaxis right
plot(data(:, 1), data(:, 2), '-*', 'MarkerSize', ms)
set(gca, 'YScale', 'log');
ylabel('$N$')
ylim([5e2 2e5])
% if i == 1
legend('$e_\infty$', '$e_1$', '$e_E$', '$N$',...
    'Location', 'NE');
% end
title(sprintf('$\\gamma = %g$', eps));

% exportf(f1, [imagepath casename '.pdf'])

i = i + 1;
end

prepare
casename = 'adaptive_3d';

load([plotdatapath casename '.mat'], 'diagonals', 'a', 'e');

x = diagonals.x;
asv = diagonals.sv;

g0 = 0.8;

n = length(diagonals.iter);
f1 = setfig('b1', [400 350]);

plot(x, asv, 'r-', 'LineWidth', 3)
leg = {'analytical'};
type = {'-', '--', '-.'};
for i = 1:n
    g = (1-i/n)*[g0 g0 g0];
    plot(x, diagonals.iter{i}, type{mod(i, 3)+1}, 'Color', g)
    leg{end+1} = sprintf('iter %d', i-1);
end

xlabel('$x = y = z$')
ticks = [-1:0.2:-0.1 -e 0];
xticks(ticks)
labels = cell(length(ticks), 1);
for i = 1:5
    labels{i} = sprintf('%.1f', ticks(i));
end
labels{6} = '-$\gamma$\ \ \ \ \ ';
labels{7} = '\ \ 0';
xticklabels(labels);
xlim([-a, 0])
ylim([0 1700])
ylabel('von Mises stress $\sigma_v$')

legend(leg, 'Location', 'NW')

% exportf(f1, [imagepath casename '_profiles.pdf']);

f2 = setfig('b3', [400 350]);

plot(x, asv, 'r-', 'LineWidth', 3)
leg = {'analytical'};
for i = 1:n
    g = (1-i/n)*[g0 g0 g0];
    plot(x, diagonals.iter{i}, type{mod(i, 3)+1}, 'Color', g)
    leg{end+1} = sprintf('iter %d', i-1);
end

xlabel('$x = y = z$')
xlim([-0.015, -e])
ylim([500 1700])
ylabel('von Mises stress $\sigma_v$')

legend(leg, 'Location', 'NW')
% exportf(f2, [imagepath casename '_profiles_zoom.pdf']);


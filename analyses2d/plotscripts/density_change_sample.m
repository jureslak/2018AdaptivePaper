prepare

load([plotdatapath 'err_ind_sample.mat'])

f1 = setfig('b1', [1000, 400]);
threshold = 0.0032;
threshold2 = 0.0007;

err_ind(err_ind > 0.014) = 0.013;
M = max(err_ind);
m = min(err_ind);
plot(err_ind, 'o', 'MarkerFaceColor', 'k', 'MarkerSize', 4, 'MarkerEdgeColor', 'k');
plot([-2.5, 100], threshold*[1 1], '--', 'LineWidth', 2)
plot([-2.5, 100], M*[1 1], '--', 'LineWidth', 2)
plot([-2.5, 100], threshold2*[1 1], 'b--', 'LineWidth', 2)
plot([-2.5, 100], m*[1 1], '--k', 'LineWidth', 2)
fill([-2.5 -2.5 100 100], [threshold M M threshold], [0 0 0], 'FaceAlpha', 0.02, 'EdgeColor', 'none')
fill([-2.5 -2.5 100 100], [threshold threshold2 threshold2 threshold], [0 0 0], 'FaceAlpha', 0.10, 'EdgeColor', 'none')
fill([-2.5 -2.5 100 100], [threshold2 m m threshold2], [0 0 0], 'FaceAlpha', 0.2, 'EdgeColor', 'none')
[~,lh] = legend(...
       '\ \ error indicator $\hat{e}$',...
       '\ \ error threshold $\varepsilon$', ...
       '\ \ maximal value $M$',...
       '\ \ error threshold $\eta$', ...
       '\ \ minimal value $m$',...
       '\ \ density increase',...
       '\ \ no change',...
       '\ \ density decrease',...
       'Location', 'bestoutside');
xlim([0, length(err_ind)+1])
xlabel('node number $i$')
ylabel('value of error indicator $\hat{e}_i$')

id = 20;
annotation('textarrow', [0.26464 0.23954]+0.020,...
           [0.82849 0.85477],'String','density increase by factor $\alpha$',...
           'Interpreter', 'LaTeX')
annotation('textarrow', [0.35983 0.38598]+0.031,...
           [0.35118 0.32397],'String','almost no density increase ',...
           'Interpreter', 'LaTeX')
text(0.2, 0.15, 'no density change', 'Units', 'normalized')
annotation('textarrow', [0.76 0.72],...
           [0.16 0.14],'String','density decrease ',...
           'Interpreter', 'LaTeX')
patches = findobj(lh, 'type', 'patch');
set(patches(1), 'facea', 0.02)
set(patches(2), 'facea', 0.1)
set(patches(3), 'facea', 0.2)
grid on
exportf(f1, [imagepath 'density_change_sample.png'])

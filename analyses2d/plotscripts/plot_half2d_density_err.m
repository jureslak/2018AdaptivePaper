prepare

i = 1;

errcliml = [-6, -6, -6];
errclim = [-1, 4, 5];
indliml = [-8, -6, -6; -9, -9, -8];
indlim = [-4, -1.5, -1; -6 -6 -6.6];
dxclim = [1.1, 1.3, 1.5];

for c = [0.2, 0.02, 0.002]
    
s = sprintf('%g', c);
casename = ['half2d_convergence_moving_' s(3:end) '_v3_iterdata'];
load([plotdatapath casename '.mat'])
f1 = setfig(sprintf('b%d', i), [850 400]);

subplot(2, 3, 1); hold on; grid on; box on;
scatter(px, py, 1, log10(first_ind), 'filled')
xlim([-inf, inf])
ylim([-inf, inf])
caxis([indliml(1, i), indlim(1, i)])
daspect([1 1 1])
colorbar
colormap jet
title('$\log_{10}(\hat{e})$, first iteration');

subplot(2, 3, 2); hold on; grid on; box on;
scatter(px, py, 1, log10(first_err), 'filled')
xlim([-inf, inf])
ylim([-inf, inf])
daspect([1 1 1])
caxis([errcliml(i), errclim(i)])
colorbar
colormap jet
title('$\log_{10}(\tilde{e}_E)$, first iteration');

subplot(2, 3, 3); hold on; grid on; box on;
scatter(px, py, 1, -log10(first_dx/max(first_dx)), 'filled')
xlim([-inf, inf])
ylim([-inf, inf])
caxis([0, dxclim(i)])
daspect([1 1 1])
colorbar
colormap jet
title('$\rho$, first iteration');

subplot(2, 3, 4); hold on; grid on; box on;
scatter(px, py, 1, log10(last_ind), 'filled')
xlim([-inf, inf])
ylim([-inf, inf])
caxis([indliml(2, i), indlim(2, i)])
daspect([1 1 1])
colorbar
colormap jet
title('$\log_{10}(\hat{e})$, last iteration');

subplot(2, 3, 5); hold on; grid on; box on;
scatter(px, py, 1, log10(last_err), 'filled')
xlim([-inf, inf])
ylim([-inf, inf])
daspect([1 1 1])
colorbar
colormap jet
caxis([errcliml(i), errclim(i)])
title('$\log_{10}(\tilde{e}_E)$, last iteration');

subplot(2, 3, 6); hold on; grid on; box on;
scatter(px, py, 1, -log10(last_dx/max(last_dx)), 'filled')
xlim([-inf, inf])
ylim([-inf, inf])
daspect([1 1 1])
caxis([0, dxclim(i)])
colorbar
colormap jet
title('$\rho$, last iteration');

% exportf(f1, [imagepath casename '.png'])

i = i + 1;
end

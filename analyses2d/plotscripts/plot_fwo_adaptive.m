prepare

casename = 'fwo_adaptive3';
load([plotdatapath casename '.mat'])
load([plotdatapath 'fwo_abaqus.mat'])
FF = importdata([plotdatapath 'freefem_surface.dat']);


testx = linspace(-2*a, 2*a, 1000);
femsxx = interp1(FF(:, 1), FF(:, 3)/1e6, testx);
abasxx = interp1(x_aba, sxx_aba, testx);

err1base = mean(abs(femsxx - abasxx)) / mean(abs(abasxx))

niter = 4;
iterdata = zeros(niter, 2);
for i = 1:niter
   
    v = von_mises(data{i}.sxx, data{i}.syy, data{i}.sxy, nu);
    
    f1 = setfig(['bo' sprintf('%d', i*4-3)], [1000, 300]);
    scatter(data{i}.x*1e3, data{i}.y*1e3, 5, v/1e6, 'filled');
    if i == 4, xlabel('$x$ [mm]'), end
    ylabel('$y$ [mm]')
    xlim([-inf, inf])
    ylim([-inf, inf])
    daspect([1 1 1])
    colormap jet
    c = colorbar;
    caxis([0, 300])
    title(c, 'MPa', 'interpreter', 'latex')
    title(sprintf('iteration %d, $N = %d$', i-1, length(data{i}.x)))
    
    y = data{i}.y;
    I = find(y == max(y));
    
%     exportf(f1, [imagepath casename '_scatter' sprintf('%d', i) '.png'])
        
    f2 = setfig(['bo' sprintf('%d', i*4-1)], [400, 300]);
    if i >= 4, ms = 3; elseif i==3, ms=6; else, ms = 10; end
    plot(x_aba*1e3, sxx_aba, '-');
    plot(FF(:, 1)*1e3, FF(:, 3)/1e6, '-');
    plot(data{i}.x(I)*1e3, data{i}.sxx(I)/1e6, '.-k', 'MarkerSize', ms);
    xlabel('$x$ [mm]')
    ylabel('$\sigma_{xx}$ [MPa]')
    xlim([-2*a*1000, 2*a*1000])
    title(sprintf('iteration %d, $N = %d$', i-1, length(data{i}.x)))
    legend('ABAQUS solution', ...
           'FreeFEM++ solution', 'adaptive solution', 'Location', 'NW')
    
    mlsmsxx = interp1(data{i}.x(I), data{i}.sxx(I)/1e6, testx);
    iterdata(i, 1) = mean(abs(mlsmsxx-abasxx)) / mean(abs(abasxx));
    iterdata(i, 2) = mean(abs(mlsmsxx-femsxx)) / mean(abs(femsxx));
    
%     exportf(f2, [imagepath casename '_top_traction' sprintf('%d', i) '.pdf'])
    
%     break
end

if 1

f2 = setfig('b2', [500, 300]);
plot(0:niter-1, iterdata(:, 1), 'o-');
plot(0:niter-1, iterdata(:, 2), 'o-');
plot([-1, niter], [err1base, err1base], '--k');
xlim([-0.5, niter-0.3]);
set(gca, 'yscale', 'log')
ylim([5e-3, 1])
legend('error against ABAQUS', 'error against FreeFEM++',...
       'reference solution variability');
ylabel('$e_1$ on contact surface $[-2a, 2a]$')
xlabel('iteration number')
% exportf(f2, [imagepath casename '_error.pdf'])

end

if 0

xx = data{4}.x;
yy = data{4}.y;
[I, dr] = knnsearch([xx yy], [xx yy], 'K', 2);
[X, Y] = meshgrid(linspace(min(xx), max(xx), 100), linspace(min(yy), max(yy), 100));
DR = griddata(xx, yy, dr(:, 2), X, Y);
f1 = setfig('b1');
contourf(X*1e3, Y*1e3, -log10(DR/max(dr(:, 2))), 100, 'EdgeColor', 'none')
daspect([1 1 1])
colorbar
colormap jet
caxis([0, 3])
xlabel('$x$ [mm]')
ylabel('$y$ [mm]')
title('$-\log_{10}(\delta r / \max \delta r)$')

end
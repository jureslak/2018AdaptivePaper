prepare

f = @(x) 3*(1-x).^2.*exp(-x.^2) + 3*exp(-4*(x-1).^2);
x = -3:0.001:3;
fx = f(x);

filename = 'adaptive_demo.h5';
datafile = [plotdatapath filename];

xd = h5read(datafile, '/pos_dense');
[xd, Id] = sort(xd);

figs = {'bo1', 'bo6', 'bo11', 'bo16', 'bo5', 'bo6', 'bo7', 'bo8', 'bo9'};

iter = 0;
while 1

folder = sprintf('/iter%02d', iter);
try
x = h5read(datafile, [folder '/pos']);
catch, break; end
[x, I] = sort(x);
N = length(x);
% fprintf('N = %d\n', N);
fx_c = h5read(datafile, [folder '/correct']);
fx_c = fx_c(I);
fx_a = h5read(datafile, [folder '/approx']);
fx_a = fx_a(I);

[~, D] = knnsearch(x, x, 'k', 2);
width = D(:, 2);

fx_cd = h5read(datafile, [folder '/correct_dense']);
fx_cd = fx_cd(Id);
fx_ad = h5read(datafile, [folder '/approx_dense']);
fx_ad = fx_ad(Id);

f1 = setfig(figs{iter+1}, [400, 300]);
plot(x, fx_c, 'xk');
plot(xd, fx_cd, '-k');
plot(x, fx_a, '*r');
plot(xd, fx_ad, '-r');
ylim([-0.75, 6])
yticks(0:1:6)
xticks(-3:1:3)
ylabel('$g$, $\hat{g}$');
title(sprintf('Iteration %d, $\\|g-\\hat{g}\\|_1 = %.3f$', iter, mean(abs(fx_cd-fx_ad))));

error = h5read(datafile, [folder '/error']); error = error(I);
% fprintf('%.6f\n', mean(error));

ref = h5readatt(datafile, folder, 'ref');
deref = h5readatt(datafile, folder, 'deref');
same = h5readatt(datafile, folder, 'same');
limit = h5readatt(datafile, folder, 'limit');
fprintf('%d & %d & %d & %d & %d & %d \\\\ \n', iter, N, ref, same, deref, limit);

if iter ~= 3
yyaxis right
ylabel('$f_i$')
yt = -3:1:3;
yticks(yt);
ylv = yt+1;
ylv(ylv<1) = -1./((ylv(ylv<1)-1)-1);
yl = cell(size(yt));
for i = 1:length(yt), yl{i} = sprintf('%.2f', ylv(i)); end
yticklabels(yl)
end


factor = h5read(datafile, [folder '/factor']); factor = factor(I);
factor = factor-1;
If = factor < 0;
factor(If) = -1./(factor(If)+1)+1;
% fprintf('%.6f\n', mean(factor));
% bar(x, error, 'FaceAlpha', 0.9, 'EdgeColor', 'none')

g = 0.7;
vd = interp1(x, factor, xd, 'nearest');
if iter == 3
vd = nan*vd;
end
plot(xd, vd, '-');
% bar(x, factor, , 'EdgeColor', 'none', 'FaceAlpha', 0.5);

if iter ~= 3
plot(x, zeros(size(factor)), '--', 'Color', [g g g]);
ylim([-3.5 3.5])
end
xlim([-3, 3])

new_dx = h5read(datafile, [folder '/new_dx']); new_dx = new_dx(I);

if iter == 3
legend('$g_i$', '$g$', '$\hat{g}_i$', '$\hat{g}$', '$f_i$', 'Location', 'NE')
end

% exportf(f1, [imagepath sprintf('adaptive_demo_iter%d.pdf', iter)]);

iter = iter+1;

end

prepare

casename = 'half2d_convergence_v4_2';

load([plotdatapath casename '.mat'])

f1 = setfig('b1', [400 400]);

% a = polyfit(log(data(1:end-5, 1)), log(data(1:end-5, 3)), 1);
b = polyfit(log(data(1:end-5, 1)), log(data(1:end-5, 5)), 1);
c = polyfit(log(data(1:end-5, 1)), log(data(1:end-5, 2)), 1);

plot(data(:, 1), data(:, 2), 'x-')
plot(data(:, 1), data(:, 3), 'o-')
% plot(data(:, 1), data(:, 4), '<-')
plot(data(:, 1), data(:, 5), '<-')
leg = {'uniform $e_\infty$', 'uniform $e_1$', 'uniform $e_E$'};


if 1

loc = strfind(casename, '_');    
casename2 = ['half2d_convergence_moving' casename(loc(end):end) '_v3'];
data2 = load([plotdatapath casename2 '.mat'], 'data');
data2 = data2.data;

plot(data2(:, 2), data2(:, 5), '*-', 'LineWidth', 2, 'Color', [0 0.2431 0.5098])
plot(data2(:, 2), data2(:, 3), 'h-', 'LineWidth', 2, 'Color', [0.6314 0.1176 0])
plot(data2(:, 2), data2(:, 6), '^-', 'LineWidth', 2, 'Color', [0.6235 0.4275 0])
leg{end+1} = 'adaptive $e_\infty$';
leg{end+1} = 'adaptive $e_1$';
leg{end+1} = 'adaptive $e_E$';

end

if strcmp(casename(end-1:end), '_2')
% plot(data(:, 1), exp(a(2))*data(:, 1).^a(1), 'k--')
plot(data(:, 1), exp(b(2))*data(:, 1).^b(1), 'k--')
plot(data(:, 1), exp(c(2))*data(:, 1).^c(1), 'k--')
% text(0.65, 0.30, sprintf('$k = %.2f$', a(1)), 'Units', 'normalized')
text(0.40, 0.1, sprintf('$k = %.2f$', b(1)), 'Units', 'normalized')
text(0.72, 0.45, sprintf('$k = %.2f$', c(1)), 'Units', 'normalized')
leg{end+1} = 'trend';

legend(leg, 'Location', 'NE');
else 
legend(leg, 'Location', 'SW');
end

set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
xlabel('$N$')
ylabel('error')
if strcmp(casename(end-3:end), '_002')
xlim([30, 10^5])
else
xlim([30, 10^4])
end
ylim([5e-5, 10])
title(sprintf('$\\gamma = %g$', eps));

% exportf(f1, [imagepath casename '.pdf'])
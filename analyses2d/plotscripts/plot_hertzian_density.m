prepare

casename = 'hertzian2';
load([plotdatapath casename '.mat'])

last = size(data, 1);
bb = 4.2;

xx = data{last}.x;
yy = data{last}.y;

I = -bb < xx/a & xx/a < bb & -bb < yy/a;
xx = xx(I);
yy = yy(I);

[~, dr] = knnsearch([xx yy], [xx yy], 'K', 2);
[X, Y] = meshgrid(linspace(min(xx), max(xx), 100), linspace(min(yy), max(yy), 100));
DR = griddata(xx, yy, dr(:, 2), X, Y);
f1 = setfig('b1', [700, 500]);
contourf(X/a, Y/a, -log10(DR/max(dr(:, 2))), 100, 'EdgeColor', 'none')
daspect([1 1 1])
colorbar
colormap jet
% caxis([0, 2])
xlabel('$x/a$')
ylabel('$y/a$')
xlim([-bb, bb])
ylim([-bb, 0.01])
set(gca,'TickDir','out');
title('$-\log_{10}(\delta r / \delta r^{\rm max})$')
min(dr(:,2))

exportf(f1, [imagepath casename '_density.png'])

load([plotdatapath 'hertzian_manual_domain_density.mat'])
assert(abs(a-b) < eps)

xx = xi';
yy = yi';

I = -bb < xx/a & xx/a < bb & -bb < yy/a;
xx = xx(I);
yy = yy(I);

% f3=setfig('b3');
scatter(xi,yi,5)
[~, dr] = knnsearch([xx yy], [xx yy], 'K', 2);
[X, Y] = meshgrid(linspace(min(xx), max(xx), 100), linspace(min(yy), max(yy), 100));
DR = griddata(xx, yy, dr(:, 2), X, Y);
f2 = setfig('b2', [700, 500]);
contourf(X/a, Y/a, -log10(DR/max(dr(:, 2))), 100, 'EdgeColor', 'none')
min(dr(:, 2))
daspect([1 1 1])
colorbar
colormap jet
% caxis([0, 3])
% caxis([0, 2.2])
xlabel('$x/a$')
ylabel('$y/a$')
xlim([-bb, bb])
ylim([-bb, 0.01])
set(gca,'TickDir','out');
title('$-\log_{10}(\delta r / \delta r^{\rm max})$')

exportf(f2, [imagepath casename '_manual_density.png'])

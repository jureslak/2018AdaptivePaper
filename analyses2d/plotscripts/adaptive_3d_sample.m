prepare
casename = 'adaptive_3d';

load([plotdatapath casename '.mat'], 'pos', 'sol', 'sv', 'e');

x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);
u = sol(:, 1);
v = sol(:, 2);
w = sol(:, 3);
f = 0;

[I, D] = knnsearch(pos, pos, 'K', 2);

f1 = setfig('b1', [500 450]);
view([100.5000   38.0000])
daspect([1 1 1])
scatter3(x+f*u, y+f*v, z+f*w, 5, sv, 'filled');
colorbar
colormap(jet)
caxis([0 1499])
title('von Mises stress $\sigma_v$')
% exportf(f1, [imagepath casename '_solution.png']);

f2 = setfig('b3', [500 450]);
b = -0.05;
I = find(b < x & b < y & b < z);
xlim([b, -e/2])
ylim([b, -e/2])
zlim([b, -e/2])
view([100.5000   38.0000])
daspect([1 1 1])
scatter3(x(I)+f*u(I), y(I)+f*v(I), z(I)+f*w(I), 5, sv(I), 'filled');
% c = colorbar;
title('von Mises stress $\sigma_v$')
colormap(jet)

% exportf(f2, [imagepath casename '_solution_zoom.png']);

prepare

casename = 'half2d_convergence_indicator_wip';
casename = 'half2d_convergence_moving_02_wip';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

data = zeros(length(info.Groups), 5);

P = h5readatt(datafile, '/conf', 'case.P');
R = h5readatt(datafile, '/conf', 'case.R');
eps = h5readatt(datafile, '/conf', 'case.eps');

lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

f1 = setfig('b1', [1800 1200]);

for iter = 0:100
    name = sprintf('/iter%04d', iter);
    
    try
        N = h5readatt(datafile, name, 'N');
    catch e
        fprintf('Done at iter %d.\n', iter);
        break
    end
    pos = h5read(datafile, [name '/domain/pos']);
    types = h5read(datafile, [name '/domain/types']);

    x = pos(1, :);
    y = pos(2, :);

    sup = h5read(datafile, [name '/domain/supp'])+1;
%     try
        sol = h5read(datafile, [name '/sol']);
%     catch
%         break
%     end
%     imb = h5read(datafile, [name '/imb']);
%      imb(imb < 3) = 0;
%     imb(imb > 7) = 7;
% 
    
%     err_mat = max(abs(analytical1d(x, alpha) - sol'))
    sxx = sol(1, :);
    syy = sol(2, :);
    sxy = sol(3, :);
    v = von_mises(sxx, syy, sxy);
    [asxx, asyy, asxy] = half2d_anal(x, y, P, R);
    av = von_mises(asxx, asyy, asxy);
    errsxx = sxx - asxx; errsyy = syy - asyy; errsxy = sxy - asxy;
       
    max_err = max([abs(errsxx); abs(errsyy); abs(errsxy)]);

    err_ind = h5read(datafile, [name '/error_indicator']);
    
%     M = spconvert(h5read(datafile, [name '/M'])');
%     rhs = h5read(datafile, [name '/rhs']);
%     sol2 = M \ rhs;
%     error = norm(sol-sol2)/norm(sol);
%     assert(error < 1e-10, 'Matlab and C++ solutions do not agree, err=%f', error);
   
    l1 = l1_norm(errsxx, errsyy, errsxy) / l1_norm(asxx, asyy, asxy);
    linf = linf_norm(errsxx, errsyy, errsxy) / linf_norm(asxx, asyy, asxy);
    
    T = delaunayTriangulation([double(x)' double(y)']);
    e = energy_norm_kernel(errsxx, errsyy, errsxy, lam, mu);
    total = energy_norm_kernel(asxx, asyy, asxy, lam, mu);
    err = sqrt(intTri(T, e) / intTri(T, total));
    
    L1kernel = L1_norm_kernel(errsxx, errsyy, errsxy);
    total = L1_norm_kernel(asxx, asyy, asxy);
    L1 = intTri(T, L1kernel) / intTri(T, total);
    
    data(iter+1, 1) = iter;
    data(iter+1, 2) = length(x);
    data(iter+1, 3) = l1;
    data(iter+1, 4) = L1;
    data(iter+1, 5) = linf;
    data(iter+1, 6) = err;
    
%     try
    modnum = 2;
    if mod(iter, modnum) == 0
        subplot(4, 4, iter/modnum+1)

        h = scatter(x, y, 5, err_ind, 'filled');
        grid on
        axis equal
        title(sprintf('itr \\#%d', iter+1));
    %     caxis([0, 1e-2])
        xlim([0, R])
        ylim([0, R])
    %     daspect([1 1 1])
        colorbar
        colormap jet
    end
%     
%     catch break; end
    
    iter=iter
%     break
end

% exportf(f1, [imagepath casename '_domains'], '-png')

f2 = setfig('b4', []);
plot(data(:, 1), data(:, 3), 'x-')
% plot(data(:, 1), data(:, 4), 'o-')
plot(data(:, 1), data(:, 5), '<-')
plot(data(:, 1), data(:, 6), 'h-')
set(gca, 'YScale', 'log');
ylabel('error')
xlabel('iteration')
yyaxis right
plot(data(:, 1), data(:, 2), '-o')
set(gca, 'YScale', 'log');
ylabel('$N$')
legend('$\ell_1$', '$\ell_\infty$', '$\|\cdot\|_E$','$N$',...
    'Location', 'NW');




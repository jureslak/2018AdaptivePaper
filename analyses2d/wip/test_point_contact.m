[X, Z] = meshgrid(-1:0.01:1, -1:0.01:0);
Y = zeros(size(X));

P = 1; E = 1; nu = 0.25;
[u, v, w] = point_contact_3d_analytical(X, Y, Z, P, E, nu);
dnorm = sqrt(u.^2+v.^2+w.^2);

close all
setfig('a2');
contourf(X, Z, log(dnorm), 'EdgeColor', 'none');
colormap jet
colorbar
axis equal

[X, Y] = meshgrid(-1:0.01:1, -1:0.01:1);
Z = zeros(size(X));

[u, v, w] = point_contact_3d_analytical(X, Y, Z, P, E, nu);
dnorm = sqrt(u.^2+v.^2+w.^2);

setfig('a1');
contourf(X, Y, log(dnorm), 'EdgeColor', 'none');
colormap jet
colorbar
axis equal

x = -1:0.01:1;
y = zeros(size(x));
z = zeros(size(x));

[u, v, w] = point_contact_3d_analytical(x, y, z, P, E, nu);
setfig('a4');
plot(x, u);
plot(x, v);
plot(x, w);
legend('u', 'v', 'w');
colormap jet
colorbar

prepare

casename = 'convergence_3d_wip';
datafile = [datapath casename '.h5'];

info = h5info(datafile);

name = '/13';

a = h5readatt(datafile, '/conf', 'num.a');
P = h5readatt(datafile, '/conf', 'case.P');
E = h5readatt(datafile, '/conf', 'case.E');
nu = h5readatt(datafile, '/conf', 'case.nu');
e = h5readatt(datafile, '/conf', 'num.eps');

% f1 = setfig('b1'); hold off
% plot_domain(datafile, [name '/domain']);


pos = h5read(datafile, [name '/domain/pos']);
N = length(pos);
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);

types = h5read(datafile, [name '/domain/types']);

sol = h5read(datafile, [name '/displ']);
u = sol(:, 1);
v = sol(:, 2);
w = sol(:, 3);
dnorm = sum(sol.^2, 2);

stress = h5read(datafile, [name, '/stress']);
sxx = stress(:, 1);
syy = stress(:, 2);
szz = stress(:, 3);
sxy = stress(:, 4);
sxz = stress(:, 5);
syz = stress(:, 6);
sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

[AX, AY, AZ] = meshgrid(deal(linspace(-a, -e, 100)));
[AU, AV, AW, ASXX, ASYY, ASZZ, ASXY, ASXZ, ASYZ] = point_contact_3d_analytical(AX, AY, AZ, P, E, nu);
U = griddata(x, y, z, u, AX, AY, AZ);
V = griddata(x, y, z, v, AX, AY, AZ);
W = griddata(x, y, z, w, AX, AY, AZ);

eu = abs(U-AU); eu = eu(:);
ev = abs(V-AV); ev = ev(:);
ew = abs(W-AW); ew = ew(:);

e1 = l1_norm(eu, ev, ew) / l1_norm(U(:), V(:), W(:))

setfig('b2'); hold off;
f=1;
scatter3(x+f*u, y+f*v, z+f*w, 15, sv, 'filled')
daspect([1 1 1])
colorbar
colormap jet


if 0

[X, Y, Z] = meshgrid(deal(linspace(-a, -e, 100)));

setfig('b4'); view(3)
title('Displacement norm $\|\vec{u}\|$')
V = griddata(x, y, z, dnorm, X, Y, Z);

contourslice(X, Y, Z, V, -1.5*e, -1.5*e, [], 20);
axis equal
colorbar
colormap('jet')


setfig('b3'); view(3)
title('Von Mises stress $\sigma_v$')

I = find(y > 0);
% scatter3(x(I), y(I), z(I), 15, sv(I))

sv(sv > 0.2) = 0.2;

V = griddata(x, y, z, sv, X, Y, Z);
contourslice(X, Y, Z, V, -1.5*e, -1.5*e, [], 20)
axis equal
colorbar
colormap('jet')
caxis([0, 0.2])

end
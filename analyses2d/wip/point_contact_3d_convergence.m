prepare

casename = 'convergence_3d_wip';
datafile = [datapath casename '.h5'];

info = h5info(datafile);

a = h5readatt(datafile, '/conf', 'num.a');
P = h5readatt(datafile, '/conf', 'case.P');
E = h5readatt(datafile, '/conf', 'case.E');
nu = h5readatt(datafile, '/conf', 'case.nu');
e = h5readatt(datafile, '/conf', 'num.eps');

[AX, AY, AZ] = meshgrid(deal(linspace(-a, -e, 100)));

ncases = length(info.Groups) - 1;

Ns = zeros(ncases, 1);
errors = zeros(ncases, 4);

for i = 1:ncases

name = sprintf('/%02d', i-1);    

pos = h5read(datafile, [name '/domain/pos']);
N = length(pos);
x = pos(:, 1);
y = pos(:, 2);
z = pos(:, 3);
Ns(i) = length(pos);

types = h5read(datafile, [name '/domain/types']);

sol = h5read(datafile, [name '/displ']);
u = sol(:, 1);
v = sol(:, 2);
w = sol(:, 3);
dnorm = sum(sol.^2, 2);

stress = h5read(datafile, [name, '/stress']);
sxx = stress(:, 1);
syy = stress(:, 2);
szz = stress(:, 3);
sxy = stress(:, 4);
sxz = stress(:, 5);
syz = stress(:, 6);
sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

% [AU, AV, AW, ASXX, ASYY, ASZZ, ASXY, ASXZ, ASYZ] = point_contact_3d_analytical(AX, AY, AZ, P, E, nu);
% U = griddata(x, y, z, u, AX, AY, AZ);
% V = griddata(x, y, z, v, AX, AY, AZ);
% W = griddata(x, y, z, w, AX, AY, AZ);
% 
% eu = abs(U-AU); eu = eu(:);
% ev = abs(V-AV); ev = ev(:);
% ew = abs(W-AW); ew = ew(:);

[au, av, aw, asxx, asyy, aszz, asxy, asxz, asyz] = point_contact_3d_analytical(x, y, z, P, E, nu);
asv = von_mises(asxx, asyy, aszz, asxy, asxz, asyz);

eu = abs(u-au); ev = abs(v-av); ew = abs(w-aw);
esxx = abs(sxx - asxx);
esyy = abs(syy - asyy);
eszz = abs(szz - aszz);
esxy = abs(sxy - asxy);
esxz = abs(sxz - asxz);
esyz = abs(syz - asyz);
estress = [esxx; esyy; eszz; esxy; esxz; esyz];
esv = abs(asv - sv);

e1 = l1_norm(eu, ev, ew) / l1_norm(au, av, aw)
einf = linf_norm(eu, ev, ew) / linf_norm(au, av, aw)

errors(i, 1) = e1;
errors(i, 2) = einf;

errors(i, 3) = sum(esv) / sum(sv);


end

setfig('b1');
plot(Ns, errors(:, 1), 'o-');
plot(Ns, errors(:, 2), 'x-');
plot(Ns, errors(:, 3), 'x-');
set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
xlabel('$N$');
title('Point contact 3D');
ylabel('error');
legend('$e_1$ displ', '$e_\infty$ displ', '$e_1$ von mises stress')
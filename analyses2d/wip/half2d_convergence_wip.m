prepare

casename = 'half2d_convergence_wip';
% casename = 'half2d_convergence_refined_wip';
% casename = 'half2d_convergence_density_fill_2_wip';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

ng = length(info.Groups);

P = h5readatt(datafile, '/conf', 'case.P');
R = h5readatt(datafile, '/conf', 'case.R');
eps = h5readatt(datafile, '/conf', 'case.eps');

lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

data = zeros(ng, 4);

f = setfig('b2');
h = scatter([1],[1],25, 'filled');
colorbar
daspect([1 1 1])
xlim([0, R-eps])
ylim([0, R-eps])
% caxis([1e-3, 1])

f4 = setfig('b4');
f3 = setfig('b3');
 
for i = ng:-1:1
% for i = 1:ng
    name = info.Groups(i).Name;
    if strcmp(name, '/conf'), continue, end
        
    pos = h5read(datafile, [name '/domain/pos']);
    x = pos(1, :);
    y = pos(2, :);
    N = length(x);
    
    types = h5read(datafile, [name '/domain/types']);
    sup = h5read(datafile, [name '/domain/supp'])+1;
    explore_domain(f, h, pos, sup, types);

%     displ = h5read(datafile, [name '/disp']);
%     u = displ(1, :);
%     v = displ(2, :);
    
% try
    stress = h5read(datafile, [name '/stress']);
    sxx = stress(1, :);
    syy = stress(2, :);
    sxy = stress(3, :);
    v = von_mises(sxx, syy, sxy);
    [asxx, asyy, asxy] = half2d_anal(x, y, P, R);
    av = von_mises(asxx, asyy, asxy);
    errsxx = sxx - asxx; errsyy = syy - asyy; errsxy = sxy - asxy;
   
    l1 = l1_norm(errsxx, errsyy, errsxy) / l1_norm(asxx, asyy, asxy);
    linf = linf_norm(errsxx, errsyy, errsxy) / linf_norm(asxx, asyy, asxy);
     
    T = delaunayTriangulation([double(x)' double(y)']);
    e = energy_norm_kernel(asxx-sxx, asyy-syy, asxy-sxy, lam, mu);
    total = energy_norm_kernel(asxx, asyy, asxy, lam, mu);
    err = sqrt(intTri(T, e) / intTri(T, total));

%     trisurf(T.ConnectivityList, x, y, total, 'EdgeColor', 'none')
%     colorbar
%     title('von Mises stress $\sigma_v$ difference to analytical')
    
    data(i, 1) = N;
    data(i, 2) = l1;
    data(i, 3) = linf;
    data(i, 4) = err;
        
    h.XData = x;
    h.YData = y;
%     h.CData = abs(v-av)/max(av);
%     h.CData = v;
%     h.CData = eind;
    title(name);
    
    if 0
    figure(f4);
    clf
    idx = find(abs(x.^2+y.^2-(R-eps)^2) < 1e-6);
    phi = atan2(y(idx), x(idx));
    [phi, I] = sort(phi);
    idx = idx(I);
    hold on
    grid on
    plot(phi, sxx(idx), 'o');
    plot(phi, syy(idx), 'x');
    plot(phi, sxy(idx), '*');
    plot(phi, asxx(idx), '-');
    plot(phi, asyy(idx), '-');
    plot(phi, asxy(idx), '-');
    legend('sxx','syy', 'sxy','asxx','asyy', 'asxy', 'Location', 'SW')
    title('radial boundary')
    xlabel('$\varphi$')

    figure(f3);
    clf;
    idx = find(x < 1e-6);
    phi = y(idx);
    [phi, I] = sort(phi);
    idx = idx(I);
    hold on
    grid on
    plot(phi, sxx(idx), 'o');
    plot(phi, syy(idx), 'x');
    plot(phi, sxy(idx), '*');
    plot(phi, asxx(idx), '-');
    plot(phi, asyy(idx), '-');
    plot(phi, asxy(idx), '-');
    title('$x=0$')
    xlabel('$y$')    
    
    end
% catch
%     warning('failed %s', name)
% end
%     pause
% break
% if i > 4, break, end
end

setfig('b1', [400 400]);
plot(data(:, 1), data(:, 3), 'o-')
plot(data(:, 1), data(:, 2), 'x-')
plot(data(:, 1), data(:, 4), '<-')
legend('$e_\infty$', '$e_1$', '$e_E$');

xlabel('$N$')
ylabel('Energy norm error')
title('Error')

set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
xlabel('$N$')
ylabel('error')
xlim([30, 10^4])
ylim([1e-4, 10])
title(sprintf('$\\gamma = %g$', eps));
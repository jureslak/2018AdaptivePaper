prepare

casename = 'half2d_convergence_indicator_wip';
casename = 'half2d_convergence_moving_02_wip';
datafile = [datapath casename '.h5'];
info = h5info(datafile);

data = zeros(length(info.Groups), 5);

P = h5readatt(datafile, '/conf', 'case.P');
R = h5readatt(datafile, '/conf', 'case.R');
eps = h5readatt(datafile, '/conf', 'case.eps');

lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

name = '/iter0000';
N = h5readatt(datafile, name, 'N');
pos = h5read(datafile, [name '/domain/pos']);
types = h5read(datafile, [name '/domain/types']);

x = pos(1, :);
y = pos(2, :);

sol = h5read(datafile, [name '/sol']);

sxx = sol(1, :);
syy = sol(2, :);
sxy = sol(3, :);
v = von_mises(sxx, syy, sxy);
[asxx, asyy, asxy] = half2d_anal(x, y, P, R);
av = von_mises(asxx, asyy, asxy);
errsxx = sxx - asxx; errsyy = syy - asyy; errsxy = sxy - asxy;
max_err = max([abs(errsxx); abs(errsyy); abs(errsxy)]);

err_ind = h5read(datafile, [name '/error_indicator']);
    

f1 = setfig('b1', [1200, 400]);
threshold = 0.0032;
threshold2 = 0.0005;

err_ind(err_ind > 0.014) = 0.013;
M = max(err_ind);
plot(err_ind, 'o', 'MarkerFaceColor', 'k', 'MarkerSize', 4, 'MarkerEdgeColor', 'k');
plot([-2.5, 100], threshold*[1 1], '--', 'LineWidth', 2)
plot([-2.5, 100], M*[1 1], '--', 'LineWidth', 2)
plot([-2.5, 100], threshold2*[1 1], '--', 'LineWidth', 2)
fill([-2.5 -2.5 100 100], [threshold M M threshold], [1 0 0], 'FaceAlpha', 0.1, 'EdgeColor', 'none')
fill([-2.5 -2.5 100 100], [threshold threshold2 threshold2 threshold], [0 0 0], 'FaceAlpha', 0.1, 'EdgeColor', 'none')
fill([-2.5 -2.5 100 100], [threshold2 0 0 threshold2], [0 0 1], 'FaceAlpha', 0.1, 'EdgeColor', 'none')
legend('error indicator $\hat{e}$', 'error threshold $\varepsilon$', ...
       'maximal indicator value $M$', 'keep same density', 'increase density',...
       'Location', 'bestoutside')  
xlim([0, length(err_ind)+1])
grid on
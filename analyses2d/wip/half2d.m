clear all
prepare

casename = 'half2d_convergence_refined_wip';
casename = 'half2d_sample_wip';
% casename = 'half2d_convergence_wip';
% casename = 'half2d_convergence_density_fill_wip';
% casename = 'half2d_convergence_moving_002_wip';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

data = [];

P = h5readatt(datafile, '/conf', 'case.P');
R = h5readatt(datafile, '/conf', 'case.R');
eps = h5readatt(datafile, '/conf', 'case.eps');
lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

name = '/000042';
% name = '/06';
name = '/000122';
% name = '/0.002000';
% name = '/0.010000';
% name = '/0.030000';
name = '/000173';
name = '/';


setfig('b3');
% plot_domain(datafile, [name '/domain']);

pos = h5read(datafile, [name '/domain/pos']);
types = h5read(datafile, [name '/domain/types']);
% sup = h5read(datafile, [name '/domain/supp'])+1;

x = pos(1, :);
y = pos(2, :);
N = length(x)

f1 = setfig('b1', []);

% displ = h5read(datafile, [name '/disp']);

% M = spconvert(h5read(datafile, [name '/M'])');
% nonrow = sum(M~=0, 2)
% rhs = h5read(datafile, [name '/rhs']);
% displ = reshape(M \ rhs, [2 N]);

% u = displ(1, :);
% v = displ(2, :);
stress = h5read(datafile, [name '/stress']);
sxx = stress(1, :);
syy = stress(2, :);
sxy = stress(3, :);

h = scatter(x, y, 15, types, 'filled');
% quiver(x, y, u, v);
% xlim([0, 1])
% ylim([0, 1])
daspect([1 1 1])
title(sprintf('$N = %d$', N))
colorbar
colormap jet

% explore_domain(f1, h, pos, sup, types);

[asxx, asyy, asxy] = half2d_anal(x, y, P, R);
av = von_mises(asxx, asyy, asxy);
v = von_mises(sxx, syy, sxy);

errsxx = sxx - asxx;
errsyy = syy - asyy;
errsxy = sxy - asxy;

l1 = l1_norm(errsxx, errsyy, errsxy) / l1_norm(asxx, asyy, asxy)
linf = linf_norm(errsxx, errsyy, errsxy) / linf_norm(asxx, asyy, asxy)

h.CData = abs(errsxx)+abs(errsxy)+abs(errsyy);
h.CData = av;
% caxis([-1, 2])
% h.CData = sqrt(u.^2+v.^2);


% setfig('b2');
% cspy(log10(abs(M)+1));
% spy(M);

setfig('b4');
idx = find(abs(x.^2+y.^2-(R-eps)^2) < 1e-6);
phi = atan2(y(idx), x(idx));
[phi, I] = sort(phi);
idx = idx(I);
hold on
plot(phi, sxx(idx), 'o');
plot(phi, syy(idx), 'x');
plot(phi, sxy(idx), '*');
plot(phi, asxx(idx), '-');
plot(phi, asyy(idx), '-');
plot(phi, asxy(idx), '-');
legend('sxx','syy', 'sxy','asxx','asyy', 'asxy', 'Location', 'SW')

setfig('b3');
idx = find(abs(x) < 1e-6);
[sy, I] = sort(y(idx));
idx = idx(I);
hold on
plot(sy, sxx(idx), 'o');
plot(sy, syy(idx), 'x');
plot(sy, sxy(idx), '*');
plot(sy, asxx(idx), '-');
plot(sy, asyy(idx), '-');
plot(sy, asxy(idx), '-');
legend('sxx','syy', 'sxy','asxx','asyy', 'asxy', 'Location', 'SW')



setfig('b2');hold off
T = delaunayTriangulation([double(x)' double(y)']);

e = energy_norm_kernel(errsxx, errsyy, errsxy, lam, mu);
total = energy_norm_kernel(asxx, asyy, asxy, lam, mu);
energy_error = intTri(T, e) / intTri(T, total)

[I,D] = knnsearch(pos', pos', 'K', 2);
d=D(1:end,2);
trisurf(T.ConnectivityList, x, y, v, 'EdgeColor', 'none')
colorbar
title('von Mises stress $\sigma_v$ difference to analytical')

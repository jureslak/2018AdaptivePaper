prepare

casename = 'half2d_convergence_moving_02_wip';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

data = zeros(length(info.Groups), 6);

P = h5readatt(datafile, '/conf', 'case.P');
R = h5readatt(datafile, '/conf', 'case.R');
eps = h5readatt(datafile, '/conf', 'case.eps');

lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

[X, Y] = meshgrid(deal(linspace(0, R-eps, 250)));
px = reshape(X, [numel(X), 1]);
py = reshape(Y, [numel(Y), 1]);
I = px.^2 + py.^2 < (R-eps)^2;
px = px(I);
py = py(I);

[asxx, asyy, asxy] = half2d_anal(px, py, P, R);
av = von_mises(asxx, asyy, asxy);

yd = linspace(0, R-eps, 3000);
xd = R-eps-yd;
[asxxd, asyyd, asxyd] = half2d_anal(xd, yd, P, R);
asvd = von_mises(asxxd, asyyd, asxyd);

f3 = setfig('b2');
plot(yd, asvd, 'r-', 'LineWidth', 2)
ylim([0, 1.5*max(asvd)])
xlim([0, R-eps])

errcliml = -6;
errclim = 4;
dxclim = 1.5; % 1.1, 1.1, 1.7

% f1 = setfig('b1', [1800 1200]);

for iter = 0:100
    name = sprintf('/iter%04d', iter);
    
    try
        N = h5readatt(datafile, name, 'N');
    catch err
        fprintf('Done at iter %d.\n', iter);
        break
    end
    pos = h5read(datafile, [name '/domain/pos']);
    types = h5read(datafile, [name '/domain/types']);

    x = pos(1, :);
    y = pos(2, :);

    sup = h5read(datafile, [name '/domain/supp'])+1;
    sol = h5read(datafile, [name '/sol']);
    sxx = sol(1, :);
    syy = sol(2, :);
    sxy = sol(3, :);
    v = von_mises(sxx, syy, sxy);
    
    psxx = sinterp(x, y, sxx, px, py);
    psyy = sinterp(x, y, syy, px, py);
    psxy = sinterp(x, y, sxy, px, py);
%     [asxx, asyy, asxy] = half2d_anal(x, y, P, R);
%     av = von_mises(asxx, asyy, asxy);

    errsxx = psxx - asxx; errsyy = psyy - asyy; errsxy = psxy - asxy;
       
    max_err = max([abs(errsxx); abs(errsyy); abs(errsxy)]);

    err_ind = h5read(datafile, [name '/error_indicator']);
    
%     M = spconvert(h5read(datafile, [name '/M'])');
%     rhs = h5read(datafile, [name '/rhs']);
%     sol2 = M \ rhs;
%     error = norm(sol-sol2)/norm(sol);
%     assert(error < 1e-10, 'Matlab and C++ solutions do not agree, err=%f', error);
   
    l1 = l1_norm(errsxx, errsyy, errsxy) / l1_norm(asxx, asyy, asxy);
    linf = linf_norm(errsxx, errsyy, errsxy) / linf_norm(asxx, asyy, asxy);
    
    T = delaunayTriangulation([double(x)' double(y)']);
    en_kernel = energy_norm_kernel(errsxx, errsyy, errsxy, lam, mu);
    total = energy_norm_kernel(asxx, asyy, asxy, lam, mu);
    total_int = intTri(T, total);
    err = sqrt(intTri(T, en_kernel) / intTri(T, total));
    
    L1kernel = L1_norm_kernel(errsxx, errsyy, errsxy);
    total = L1_norm_kernel(asxx, asyy, asxy);
    L1 = intTri(T, L1kernel) / intTri(T, total);
    
    data(iter+1, 1) = iter;
    data(iter+1, 2) = length(x);
    data(iter+1, 3) = l1;
    data(iter+1, 4) = L1;
    data(iter+1, 5) = linf;
    data(iter+1, 6) = err;
    
%     try
%     modnum = 2;
%     if mod(iter, modnum) == 0
%         subplot(4, 4, iter/modnum+1)

%         h = scatter(x, y, 5, err_ind, 'filled');
%         grid on
%         axis equal
%         title(sprintf('itr \\#%d', iter+1));
    %     caxis([0, 1e-2])
%         xlim([0, R])
%         ylim([0, R])
    %     daspect([1 1 1])
%         colorbar
%         colormap jet
%     end
%     
%     catch break; end
    
    figure(f3);
    F = scatteredInterpolant(x', y', v');
    vd = F(xd, yd);
    cmap = flipud(colormap('gray'));
    plot(yd, vd, 'Color', cmap(5*iter+20, :))
    

    if iter == 0  
        f1 = setfig('b1');
        subplot(2, 2, 1); hold on; grid on; box on;
        scatter(px, py, 5, log10(en_kernel), 'filled')
        xlim([0, R])
        ylim([0, R])
        daspect([1 1 1])
        caxis([errcliml, errclim])
        colorbar
        colormap jet
        title('Error in first iteration');

        subplot(2, 2, 3); hold on; grid on; box on;
        [~, D] = knnsearch(pos', pos', 'K', 2);
        Fe = scatteredInterpolant(pos(1, :)', pos(2, :)', D(:, 2));
        pe = Fe(px, py);
        scatter(px, py, 5, -log10(pe/max(pe)), 'filled')
        xlim([0, R])
        ylim([0, R])
        caxis([0, dxclim])
        daspect([1 1 1])
        colorbar
        colormap jet
        title('Node density in first iteration');
    end


    iter=iter
%     break
end

figure(f3);
leg = cell(iter+1, 1);
leg{1} = 'analytical';
for i = 2:iter+1
    leg{i} = sprintf('iter \\#%d', i-2);
end
legend(leg);

figure(f1);
subplot(2, 2, 2); hold on; grid on; box on;
scatter(px, py, 5, log10(en_kernel), 'filled')
xlim([0, R])
ylim([0, R])
daspect([1 1 1])
colorbar
colormap jet
caxis([errcliml, errclim])
title('Error in last iteration');

subplot(2, 2, 4); hold on; grid on; box on;
[~, D] = knnsearch(pos', pos', 'K', 2);
Fe = scatteredInterpolant(pos(1, :)', pos(2, :)', D(:, 2));
pe = Fe(px, py);
scatter(px, py, 5, -log10(pe/max(pe)), 'filled')
xlim([0, R])
ylim([0, R])
daspect([1 1 1])
caxis([0, dxclim])
colorbar
colormap jet
title('Node density in last iteration');


% exportf(f1, [imagepath casename '_domains'], '-png')

f2 = setfig('b4', []);
plot(data(:, 1), data(:, 5), 'x-')
% plot(data(:, 1), data(:, 4), 'o-')
plot(data(:, 1), data(:, 3), '<-')
plot(data(:, 1), data(:, 6), 'h-')
set(gca, 'YScale', 'log');
ylabel('error')
xlabel('iteration')
yyaxis right
plot(data(:, 1), data(:, 2), '-o')
set(gca, 'YScale', 'log');
ylabel('$N$')
legend('$\ell_\infty$', '$\ell_1$', '$\|\cdot\|_E$','$N$',...
    'Location', 'NW');

f3 = setfig('b3');
plot(data(:, 2), data(:, 5), 'o-');
plot(data(:, 2), data(:, 3), 'o-');
plot(data(:, 2), data(:, 6), 'o-');
set(gca, 'YScale', 'log');
set(gca, 'XScale', 'log');

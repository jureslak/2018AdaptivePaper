prepare

filename = 'fwo_sample_wip';
% filename = 'fwo_adaptive2';
% filename = 'hertzian_sample_wip';

datafile = [datapath filename '.h5'];
info = h5info(datafile);

name = '/';

a = h5readatt(datafile, '/conf', 'case.a');
nu = h5readatt(datafile, '/conf', 'phy.nu');
p0 = h5readatt(datafile, '/conf', 'phy.p0');

f1 = setfig('b1', [1800 1200]);
f2 = setfig('b1', [1800 1200]);


for iter = 0:100

name = sprintf('/iter%04d', iter);

try
pos = h5read(datafile, [name '/domain/pos']);
catch, break, end
x = pos(1, :)';
y = pos(2, :)';
N = length(x)
types = h5read(datafile, [name '/domain/types']);

displ = h5read(datafile, [name '/displ']);

% M = spconvert(h5read(datafile, [name '/M'])');
% nonrow = sum(M~=0, 2)
% rhs = h5read(datafile, [name '/rhs']);
% displ = reshape(M \ rhs, [2 N]);

uu = displ(1, :);
vv = displ(2, :);
dnorm = sqrt(uu.^2 + vv.^2);
stress = h5read(datafile, [name '/stress']);
sxx = stress(1, :);
syy = stress(2, :);
sxy = stress(3, :);
v = von_mises(sxx, syy, sxy);

I = -3*a < x & x < 3*a & -3*a < y; 

try
dx = h5read(datafile, [name '/dx'])';
catch
    dx = ones(size(x));
end
err_ind = h5read(datafile, [name '/error_indicator'])';


% err = max(max([abs(sxx(I)-asxx'); abs(syy(I)-asyy'); abs(sxy(I)-asxy')])) / p0;

modnum = 1;
if mod(iter, modnum) == 0
idx = iter/modnum+1;
figure(f1)
subplot(5, 1, idx); grid on; box on; hold on;

% h = scatter(x/a, y/a, 5, -log(dx/max(dx))/log(10), 'filled');
h = scatter(x/a, y/a, 5, v, 'filled');
% quiver(x, y, u, v);
% xlim([0, 1])
% ylim([0, 1])
% daspect([1 1 1])
xlabel('$x/a$')
ylabel('$y/a$')
daspect([1 1 1])
% title(sprintf('von Mises stress $\\sigma_v$, $N = %d$ nodes', N))
% caxis([0 300])
c = colorbar;
title(c, 'MPa', 'Interpreter', 'LaTeX')
colormap jet
title(sprintf('iter %d, $N = %d$', iter, N))


figure(f2)
subplot(4, 4, idx); grid on; box on; hold on;
II = find(types==-1);
[ox, J] = sort(x(II));
II = II(J);
plot(ox/a, sxx(II)/1e6, '-')
xlim([-4, 4])
xlabel('$x/a$')
ylabel('$y/a$')
title(sprintf('iter %d, $N = %d$', iter, N))

end
end

setfig('b2');hold off

f = 5e3;
scatter((x(I)+f*uu(I)')/a, (y(I)+f*vv(I)')/a, 5, v(I)/p0, 'filled');
% scontour(x(I)/a, y(I)/a, v(I)/p0, 100, 100, 50);
xlim([-3, 3])
ylim([-3, 0])
% T = delaunayTriangulation([x, y]);
% trisurf(T.ConnectivityList, x, y, v/1e6, 'EdgeColor', 'none')
colorbar
colormap jet
axis equal
title('von Mises stress $\sigma_v$')


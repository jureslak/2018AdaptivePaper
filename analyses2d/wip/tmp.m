[X, Y, Z] = meshgrid(deal(linspace(-0.05, -0.01, 50)));
X=X(:);
Y=Y(:);
Z=Z(:);

P = -1;
E = 1;
nu = 0.33;
[ux, uy, uz, sxx, syy, szz, sxy, sxz, syz] = point_contact_3d_analytical(X, Y, Z, P, E, nu);
sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

close all
setfig('b1');
view(3)
scatter3(X, Y, Z, 5, sv, 'filled');
daspect([1 1 1])
view([100.5000   38.0000])
colormap jet
colorbar

prepare

casename = 'half2d_convergence_moving_alpha_analysis_wip';
% casename = 'half2d_convergence_refined_wip';
% casename = 'half2d_convergence_density_fill_2_wip';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

ng = length(info.Groups)-2;

P = h5readatt(datafile, '/conf', 'case.P');
R = h5readatt(datafile, '/conf', 'case.R');
eps = h5readatt(datafile, '/conf', 'case.eps');

lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

data = zeros(ng, 6);

for i = ng:-1:1
    name = info.Groups(i).Name;
    iter = length(info.Groups(i).Groups);
    final = info.Groups(i).Groups(end).Name;
    N = h5readatt(datafile, final, 'N');    
    
    try
        c = h5readatt(datafile, final, 'converged');    
    catch
        c = 1;
    end
    
    
    pos = h5read(datafile, [final '/domain/pos']);
    x = pos(1, :);
    y = pos(2, :);
    N = length(x);
    
    stress = h5read(datafile, [final '/sol']);
    sxx = stress(1, :);
    syy = stress(2, :);
    sxy = stress(3, :);
    v = von_mises(sxx, syy, sxy);
    [asxx, asyy, asxy] = half2d_anal(x, y, P, R);
    av = von_mises(asxx, asyy, asxy);
    errsxx = sxx - asxx; errsyy = syy - asyy; errsxy = sxy - asxy;
   
    l1 = l1_norm(errsxx, errsyy, errsxy) / l1_norm(asxx, asyy, asxy);
    linf = linf_norm(errsxx, errsyy, errsxy) / linf_norm(asxx, asyy, asxy);
     
    T = delaunayTriangulation([double(x)' double(y)']);
    e = energy_norm_kernel(asxx-sxx, asyy-syy, asxy-sxy, lam, mu);
    total = energy_norm_kernel(asxx, asyy, asxy, lam, mu);
    err = sqrt(intTri(T, e) / intTri(T, total));
    
   
    
    alpha = str2double(name(7:end));
    data(i, 1) = alpha;
    if c, data(i, 2) = iter; else, data(i, 2) = nan; end
    data(i, 3) = N;
    
    data(i, 4) = l1;
    data(i, 5) = linf;
    data(i, 6) = err;
end

[~,I] = sort(data(:, 1));
data = data(I, :);

setfig('b1');
plot(data(:, 1), data(:, 2), 'o-')
% legend('$$\|\cdot\|_1$', '$\|\cdot\|_\infty$', '$\|\cdot\|_E$');
set(gca, 'xscale', 'log')
% set(gca, 'yscale', 'log')
xlabel('$\alpha$')
xticks(data(:, 1))
ylabel('iterations until convergence')
% title('Error')

f = setfig('b2');
plot(data(:, 1), data(:, 3), 'o-')
% legend('$$\|\cdot\|_1$', '$\|\cdot\|_\infty$', '$\|\cdot\|_E$');
set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
xticks(data(:, 1))
xlabel('$\alpha$')
ylabel('Number of nodes in final iteration')

f = setfig('b3');
plot(data(:, 1), data(:, 4), 'o-')
plot(data(:, 1), data(:, 5), 'o-')
plot(data(:, 1), data(:, 6), 'o-')
legend('$$\|\cdot\|_1$', '$\|\cdot\|_\infty$', '$\|\cdot\|_E$');
set(gca, 'xscale', 'log')
set(gca, 'yscale', 'log')
xticks(data(:, 1))
xlabel('$\alpha$')
ylabel('Error in final iteration')
% title('Error')

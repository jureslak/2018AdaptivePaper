prepare

% casename = 'half2d_convergence_indicator_wip';

casename = 'half2d_convergence_moving_2_v3';
datafile = [datapath casename '.h5'];
info = h5info(datafile);

data = zeros(length(info.Groups)-1, 5);

P = h5readatt(datafile, '/conf', 'case.P');
R = h5readatt(datafile, '/conf', 'case.R');
eps = h5readatt(datafile, '/conf', 'case.eps');

lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

[X, Y] = meshgrid(deal(linspace(0, R-eps, 250)));
px = reshape(X, [numel(X), 1]);
py = reshape(Y, [numel(Y), 1]);
I = px.^2 + py.^2 < (R-eps)^2;
px = px(I);
py = py(I);
T = delaunayTriangulation([px py]);

[asxx, asyy, asxy] = half2d_anal(px, py, P, R);
av = von_mises(asxx, asyy, asxy);

yd = linspace(0, R-eps, 1000);
xd = R-eps-yd;

profiles.x = yd;
profiles.y = yd;
[profiles.asxx, profiles.asyy, profiles.asxy] = half2d_anal(xd, yd, P, R);
profiles.iter = {};

for iter = 0:100
    name = sprintf('/iter%04d', iter);
    
    try
        N = h5readatt(datafile, name, 'N');
    catch e
        fprintf('Done at iter %d.\n', iter);
        break
    end
    pos = h5read(datafile, [name '/domain/pos']);
    types = h5read(datafile, [name '/domain/types']);
    
    x = pos(1, :);
    y = pos(2, :);
    
    sol = h5read(datafile, [name '/sol']);
    sxx = sol(1, :);
    syy = sol(2, :);
    sxy = sol(3, :);
    v = von_mises(sxx, syy, sxy);
    
    psxx = sinterp(x, y, sxx, px, py);
    psyy = sinterp(x, y, syy, px, py);
    psxy = sinterp(x, y, sxy, px, py);
    
    errsxx = psxx - asxx; errsyy = psyy - asyy; errsxy = psxy - asxy;
    
    max_err = max([abs(errsxx); abs(errsyy); abs(errsxy)]);
    err_ind = h5read(datafile, [name '/error_indicator']);
    pind = sinterp(x, y, err_ind, px, py);
    
    l1 = l1_norm(errsxx, errsyy, errsxy) / l1_norm(asxx, asyy, asxy);
    linf = linf_norm(errsxx, errsyy, errsxy) / linf_norm(asxx, asyy, asxy);
    
    en_kernel = energy_norm_kernel(errsxx, errsyy, errsxy, lam, mu);
    total = energy_norm_kernel(asxx, asyy, asxy, lam, mu);
    err = sqrt(intTri(T, en_kernel)) / sqrt(intTri(T, total));
    
    L1kernel = L1_norm_kernel(errsxx, errsyy, errsxy);
    total = L1_norm_kernel(asxx, asyy, asxy);
    L1 = intTri(T, L1kernel) / intTri(T, total);
    
    data(iter+1, 1) = iter;
    data(iter+1, 2) = length(x);
    data(iter+1, 3) = l1;
    data(iter+1, 4) = L1;
    data(iter+1, 5) = linf;
    data(iter+1, 6) = err;
    
    profiles.iter{end+1}.sxx = sinterp(x, y, sxx, xd, yd);
    profiles.iter{end}.syy = sinterp(x, y, syy, xd, yd);
    profiles.iter{end}.sxy = sinterp(x, y, sxy, xd, yd);
    
    if iter == 0
        [~, D] = knnsearch(pos', pos', 'K', 2);
        F = scatteredInterpolant(pos(1, :)', pos(2, :)', D(:, 2));
        first_dx = F(px, py);
        first_err = en_kernel / intTri(T, total);
        first_ind = pind;
    end
    fprintf('iter = %d\n', iter);
end

[~, D] = knnsearch(pos', pos', 'K', 2);
F = scatteredInterpolant(pos(1, :)', pos(2, :)', D(:, 2));
last_dx = F(px, py);
last_err = en_kernel / intTri(T, total);
last_ind = pind;

save([plotdatapath casename '.mat'], 'data', 'eps')
save([plotdatapath casename '_iterdata.mat'], 'px', 'py', ...
    'first_dx', 'last_dx', 'first_err', 'last_err', ...
    'first_ind', 'last_ind', 'eps')
save([plotdatapath casename '_profiles.mat'], 'profiles', 'eps')
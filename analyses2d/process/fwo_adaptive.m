prepare

casename = 'fwo_adaptive3';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

name = '/';

a = h5readatt(datafile, '/conf', 'case.a');
nu = h5readatt(datafile, '/conf', 'phy.nu');

data = cell(5, 1);

for iter = 0:100

name = sprintf('/iter%04d', iter);

try
pos = h5read(datafile, [name '/domain/pos']);
catch, break, end
x = pos(1, :)';
y = pos(2, :)';
N = length(x)
types = h5read(datafile, [name '/domain/types']);

displ = h5read(datafile, [name '/displ']);

% M = spconvert(h5read(datafile, [name '/M'])');
% nonrow = sum(M~=0, 2)
% rhs = h5read(datafile, [name '/rhs']);
% displ = reshape(M \ rhs, [2 N]);

uu = displ(1, :);
vv = displ(2, :);
dnorm = sqrt(uu.^2 + vv.^2);
stress = h5read(datafile, [name '/stress']);
sxx = stress(1, :);
syy = stress(2, :);
sxy = stress(3, :);
v = von_mises(sxx, syy, sxy, nu);

data{iter+1}.x = x;
data{iter+1}.y = y;
data{iter+1}.sxx = sxx;
data{iter+1}.syy = syy;
data{iter+1}.sxy = sxy;

end

save([plotdatapath casename '.mat'], 'data', 'a', 'nu')
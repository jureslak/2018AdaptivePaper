prepare

casename = 'half2d_convergence_moving_alpha_analysis';
% casename = 'half2d_convergence_refined_wip';
% casename = 'half2d_convergence_density_fill_2_wip';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

ng = length(info.Groups)-2;

P = h5readatt(datafile, '/conf', 'case.P');
R = h5readatt(datafile, '/conf', 'case.R');
eps = h5readatt(datafile, '/conf', 'case.eps');

lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

data = zeros(ng, 6);

% mesh for error evaluation
[X, Y] = meshgrid(deal(linspace(0, R-eps, 250)));
px = reshape(X, [numel(X), 1]);
py = reshape(Y, [numel(Y), 1]);
I = px.^2 + py.^2 < (R-eps)^2;
px = px(I);
py = py(I);
T = delaunayTriangulation([px py]);

[asxx, asyy, asxy] = half2d_anal(px, py, P, R);
av = von_mises(asxx, asyy, asxy);

for i = ng:-1:1
    name = info.Groups(i).Name;
    iter = length(info.Groups(i).Groups);
    final = info.Groups(i).Groups(end).Name;
    N = h5readatt(datafile, final, 'N');    
    
    try
        c = h5readatt(datafile, final, 'converged');    
    catch
        c = 1;
    end
    
    
    pos = h5read(datafile, [final '/domain/pos']);
    x = pos(1, :);
    y = pos(2, :);
    N = length(x);
    
    stress = h5read(datafile, [final '/sol']);
    sxx = stress(1, :);
    syy = stress(2, :);
    sxy = stress(3, :);
    v = von_mises(sxx, syy, sxy);
    
    psxx = sinterp(x, y, sxx, px, py);
    psyy = sinterp(x, y, syy, px, py);
    psxy = sinterp(x, y, sxy, px, py);
    
    errsxx = psxx - asxx; errsyy = psyy - asyy; errsxy = psxy - asxy;
   
    l1 = l1_norm(errsxx, errsyy, errsxy) / l1_norm(asxx, asyy, asxy);
    linf = linf_norm(errsxx, errsyy, errsxy) / linf_norm(asxx, asyy, asxy);
    
    en_kernel = energy_norm_kernel(errsxx, errsyy, errsxy, lam, mu);
    total = energy_norm_kernel(asxx, asyy, asxy, lam, mu);
    err = sqrt(intTri(T, en_kernel) / intTri(T, total));
    
    L1kernel = L1_norm_kernel(errsxx, errsyy, errsxy);
    total = L1_norm_kernel(asxx, asyy, asxy);
    L1 = intTri(T, L1kernel) / intTri(T, total);
    
    alpha = str2double(name(7:end));
    data(i, 1) = alpha;
    if c, data(i, 2) = iter; else, data(i, 2) = nan; end
    if c, data(i, 3) = N; else, data(i, 3) = nan; end
    
    data(i, 4) = l1;
    data(i, 5) = linf;
    data(i, 6) = err;
end

[~,I] = sort(data(:, 1));
data = data(I, :);

save([plotdatapath casename '.mat'], 'data');
prepare

casename = 'adaptive_3d';

datafile = [datapath casename '.h5'];
info = h5info(datafile);

a = h5readatt(datafile, '/conf', 'num.a');
P = h5readatt(datafile, '/conf', 'case.P');
E = h5readatt(datafile, '/conf', 'case.E');
nu = h5readatt(datafile, '/conf', 'case.nu');
e = h5readatt(datafile, '/conf', 'num.eps');

lam = h5readatt(datafile, '/conf', 'case.lam');
mu = h5readatt(datafile, '/conf', 'case.mu');

xd = linspace(-a, -e, 1000);
yd = xd; zd = xd;

diagonals.iter = {};
[ud, vd, wd, sxxd, syyd, szzd, sxyd, sxzd, syzd] = point_contact_3d_analytical(xd, yd, yd, P, E, nu);
svd = von_mises(sxxd, syyd, szzd, sxyd, sxzd, syzd);
diagonals.x = xd;
diagonals.y = yd;
diagonals.z = zd;
diagonals.sv = svd;

% [X, Y, Z] = meshgrid(deal(linspace(-a, -e, 100)));
[X, Y, Z] = meshgrid(deal(linspace(-a, -e, 10)));
[au, av, aw, asxx, asyy, aszz, asxy, asxz, asyz] = point_contact_3d_analytical(X, Y, Z, P, E, nu);
asv = von_mises(asxx, asyy, aszz, asxy, asxz, asyz);
T = delaunayTriangulation([X(:) Y(:) Z(:)]);

data = cell(length(info.Groups)-2, 1);
for i = 1:100
    name = sprintf('/%03d', i-1);
    disp(name)

    try sol = h5read(datafile, [name '/displ']); catch, break; end

    try pos = h5read(datafile, [name '/domain/pos']); catch, break; end
    N = length(pos);
    x = pos(:, 1);
    y = pos(:, 2);
    z = pos(:, 3);
    types = h5read(datafile, [name '/domain/types']);

    try sol = h5read(datafile, [name '/displ']); catch, break; end
    u = sol(:, 1);
    v = sol(:, 2);
    w = sol(:, 3);
    dnorm = sum(sol.^2, 2);

    stress = h5read(datafile, [name, '/stress']);
    sxx = stress(:, 1);
    syy = stress(:, 2);
    szz = stress(:, 3);
    sxy = stress(:, 4);
    sxz = stress(:, 5);
    syz = stress(:, 6);
    sv = von_mises(sxx, syy, szz, sxy, sxz, syz);

    F = scatteredInterpolant(x, y, z, sv, 'linear');
    csvd = F(xd, yd, zd);
    diagonals.iter{end+1} = csvd;

    F.Values = u; U = F(X, Y, Z); eu = abs(au-U); eu = eu(:);
    F.Values = v; V = F(X, Y, Z); ev = abs(av-V); ev = ev(:);
    F.Values = w; W = F(X, Y, Z); ew = abs(aw-W); ew = ew(:);
    edisp = [eu; ev; ew];
    adisp = [au(:); av(:); aw(:)];

    F.Values = sxx; SXX = F(X, Y, Z); esxx = abs(asxx-SXX); esxx = esxx(:);
    F.Values = syy; SYY = F(X, Y, Z); esyy = abs(asyy-SYY); esyy = esyy(:);
    F.Values = szz; SZZ = F(X, Y, Z); eszz = abs(aszz-SZZ); eszz = eszz(:);
    F.Values = sxy; SXY = F(X, Y, Z); esxy = abs(asxy-SXY); esxy = esxy(:);
    F.Values = sxz; SXZ = F(X, Y, Z); esxz = abs(asxz-SXZ); esxz = esxz(:);
    F.Values = syz; SYZ = F(X, Y, Z); esyz = abs(asyz-SYZ); esyz = esyz(:);
    estress = [esxx; esyy; eszz; esxy; esxz; esyz];
    astress = [asxx(:); asyy(:); aszz(:); asxy(:); asxz(:); asyz(:)];

    e1u = sum(edisp) / sum(abs(adisp));
    einfu = max(edisp) / max(abs(adisp));
    e1s = sum(estress) / sum(abs(astress));

    en_kernel = energy_norm_kernel3(esxx, esyy, eszz, esxy, esxz, esyz, lam, mu);
    total = energy_norm_kernel3(asxx(:), asyy(:), aszz(:), asxy(:), asxz(:), asyz(:), lam, mu);
    err = sqrt(intTri3(T, en_kernel)) / sqrt(intTri3(T, total));

    einfs = max(estress) / max(abs(astress));

    data{i}.N = N;
    data{i}.e1u = e1u;
    data{i}.einfu = einfu;
    data{i}.e1s = e1s;
    data{i}.einfs = einfs;
    data{i}.ees = err;

    try dx = h5read(datafile, [name, '/dx']); catch, break; end


end
fprintf('Finished after %d iterations.\n', i-1);

%%

save([plotdatapath casename '.mat'], 'data', 'diagonals', 'a', 'e', 'pos', 'sol', 'sv');

% setfig('b3');
% plot(data(:, 1), data(:, 2), '-o');
% plot(data(:, 1), data(:, 3), '-x');
% plot(data(:, 1), data(:, 4), '-v');
% plot(data(:, 1), data(:, 5), '-+');
% plot(data(:, 1), data(:, 6), '-*');
% 
% legend('$e_1$ disp', '$e_\infty$ disp', '$e_1$ stress', '$e_\infty$ stress')
% set(gca, 'xscale', 'log');
% set(gca, 'yscale', 'log');
% xlabel('$N$')
% ylabel('error')


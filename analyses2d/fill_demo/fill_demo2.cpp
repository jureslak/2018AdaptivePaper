#include "io.hpp"
#include "kdtree.hpp"
#include "domain_fill_engines.hpp"
#include "domain_extended.hpp"
#include "domain_relax_engines.hpp"


using namespace Eigen;
using namespace std;
using namespace mm;

template <typename D, typename T>
void fill(D& domain, const T& f, bool write, HDF5IO& file, int iter, const XMLloader& conf) {
    domain.clear();

    Vec2d p = {-3, -3};
    double r = f({3, -3});
    while (p[0] < 3-r) {
        domain.addBoundaryPoint(p, -1, {0, 1});
        p[0] += f(p);
    }
    p = {-3, 3-f({-3, -3})};
    r = f({-3, 3});
    while (p[1] < 3-r) {
        domain.addBoundaryPoint(p, -1, {0, 1});
        p[1] += f(p);
    }
    p = {-3, 3};
    r = f({3, 3});
    while (p[0] < 3-r) {
        domain.addBoundaryPoint(p, -1, {0, 1});
        p[0] += f(p);
    }
    p = {3, -3};
    r = f({3, 3});
    while (p[1] < 3-r) {
        domain.addBoundaryPoint(p, -1, {0, 1});
        p[1] += f(p);
    }

    Timer t;

    int seed = conf.get<int>("params.fill.seed");
    double pr = conf.get<double>("params.fill.proximity_relax");
    PoissonDiskSamplingFill fill;
    fill.seed(seed).proximity_relax(pr).randomize(false).internal_type(1);

    t.addCheckPoint("fill_start");
    domain.apply(fill, f);
    t.addCheckPoint("fill_end");

    prn(domain.size());

    string folder = format("/%04d", iter);
    if (write) {
        file.reopenFile();
        file.openFolder(folder);
        file.setDouble2DArray("filled", domain.positions);
//    file.setDoubleArray("filled_types", domain.types);
        file.closeFile();
    }

    if (write) {
    int riter = conf.get<int>("params.relax.riter");
    if (riter > 0) {
        auto domain2 = domain.makeClone(domain);
        int num_neighbours = conf.get<int>("params.relax.num_neighbours");
        double init_heat = conf.get<double>("params.relax.init_heat");
        double final_heat = conf.get<double>("params.relax.final_heat");
        BasicRelax relax;
        relax.iterations(riter).projectionType(BasicRelax::DO_NOT_PROJECT).numNeighbours(num_neighbours)
                .initialHeat(init_heat).finalHeat(final_heat);
        t.addCheckPoint("relax_start");
        domain2.apply(relax, f);
        t.addCheckPoint("relax_end");

        file.reopenFile();
        file.reopenFolder();
        file.openFolder(folder);
        file.setDouble2DArray("relaxed", domain2.positions);
        file.setTimer("timer", t);
//    file.setDoubleArray("relaxed_types", domain.types);
        file.closeFile();
    }
    }
}

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XMLloader conf(argv[1]);

    double Dx = conf.get<double>("params.fill.Dx");
    double dx = conf.get<double>("params.fill.dx");
    auto peaks = [](const Vec2d& v) {
        double x = v[0], y = v[1];
        return 3*(1-x)*(1-x)*std::exp(-x*x - (y+1)*(y+1)) - 10*(x/5 - x*x*x - y*y*y*y*y)*std::exp(-x*x-y*y) - 1./3*std::exp(-(x+1)*(x+1) - y*y);
    };

    auto dr = [=](const Vec2d& v) {
        return dx + (peaks(v) + 6.55) / 14.66 * (Dx - dx);
    };

    RectangleDomain<Vec2d> domain(-3, 3);

    auto filename = conf.get<string>("params.meta.out_file");
    HDF5IO file;
    file.openFile(filename, HDF5IO::DESTROY);
    fill(domain, dr, true, file, 0, conf);

    int iter = conf.get<int>("params.sim.iter");
    for (int i = 1; i <= iter; ++i) {
        prn(i);
        prn(domain.size());

        domain.findSupport(2);

        Range<double> new_dx(domain.size());
        for (int i = 0; i < domain.size(); ++i) {
            new_dx[i] = std::sqrt(domain.distances[i][1]);
        }
        prn(*min_element(new_dx.begin(), new_dx.end()));
        ModifiedSheppardsScatteredInterpolant<Vec2d, double> f(domain.positions, new_dx, 5);
        fill(domain, f, i == 0 || i == 1 || i == iter-1, file, i, conf);
//          f = NNInterpolant<Vec2d, double>(domain.positions, new_dx);
        prn("done", i);
    }



    return 0;
}


prepare 

peaks = @(x, y) 3*(1-x).^2.*exp(-(x.^2) - (y+1).^2) ... 
   - 10*(x/5 - x.^3 - y.^5).*exp(-x.^2-y.^2) ... 
   - 1/3*exp(-(x+1).^2 - y.^2);

dx = 0.001;
Dx = 0.05;
dr = @(x, y) dx + (peaks(x, y) + 6.55) / 14.66 * (Dx - dx);

sp = linspace(-3, 3, 250);
[X, Y] = meshgrid(sp, sp);
V = dr(X, Y);

figsize = [400, 400];
zoomin1x = [-0.6 0.3];
zoomin1y = [-1.5 -0.6];

f1 = setfig('b1', figsize);
contourf(X, Y, V, 100, 'Edgecolor', 'none');
rectangle('Position', [zoomin1x(1) zoomin1y(1) diff(zoomin1x) diff(zoomin1y)], ...
    'EdgeColor', 'w');
colormap gray
xticks([-3 -2 -1 0 1 2 3])
caxis([dx Dx])
axis equal
colorbar

f2 = setfig('b2', figsize);
contourf(X, Y, V, 100, 'Edgecolor', 'none');
colormap gray
axis equal
xlim(zoomin1x);
ylim(zoomin1y);
colorbar
caxis([dx Dx])

imagepath = 'images/';
% exportf(f1, [imagepath 'sample.png'], '-png');
% exportf(f2, [imagepath 'sample_zoom.png'], '-png');
close all
clear

datafile = 'fill2.h5';
pts = h5read(datafile, '/0000/filled');
x = pts(1, :);
y = pts(2, :);
N = length(x)

figsize = [400, 400];
zoomin1x = [-0.6 0.3];
zoomin1y = [-1.5 -0.6];

f1 = setfig('b1', figsize);
scatter(x, y, 2, 'k', 'filled')
daspect([1 1 1])
xlim([-3 3])
ylim([-3 3])
set(gca,'TickDir','out')

f3 = setfig('b3', figsize);
scatter(x, y, 3, 'k', 'filled')
daspect([1 1 1])
xlim(zoomin1x)
ylim(zoomin1y)
set(gca,'TickDir','out')

ptsr = h5read(datafile, '/0000/relaxed');
xr = ptsr(1, :);
yr = ptsr(2, :);
Nr = length(xr)

f2 = setfig('b2', figsize);
scatter(xr, yr, 2, 'k', 'filled')
daspect([1 1 1])
xlim([-3 3])
ylim([-3 3])
set(gca,'TickDir','out')

f4 = setfig('b4', figsize);
scatter(xr, yr, 3, 'k', 'filled')
daspect([1 1 1])
xlim(zoomin1x)
ylim(zoomin1y)
set(gca,'TickDir','out')

imagepath = 'images/';
% exportf(f1, [imagepath 'sample_filled_full.png']);
% exportf(f3, [imagepath 'sample_filled_zoom.png'], '-png');
% exportf(f2, [imagepath 'sample_relaxed_full.png']);
% exportf(f4, [imagepath 'sample_relaxed_zoom.png'], '-png');

peaks = @(x, y) 3*(1-x).^2.*exp(-(x.^2) - (y+1).^2) ...
   - 10*(x/5 - x.^3 - y.^5).*exp(-x.^2-y.^2) ...
   - 1/3*exp(-(x+1).^2 - y.^2);

dx = 0.007;
Dx = 0.07;
drf = @(x, y) dx + (peaks(x, y) + 6.55) / 14.66 * (Dx - dx);

pos = [x' y'];
dra = drf(x', y');
[~, D] = knnsearch(pos, pos, 'K', 7);
lg = D(:, end) ./ dra;
I = 1.5 <= lg & lg <= 1.8;

fo2 = setfig('bo6', [400 400]);
scatter(x(I), y(I), 5, 'ro', 'filled');
axis equal
title('Nodes contributing to $d_{i,j} \in [1.5, 1.8]$');

fo2 = setfig('bo10', [400 400]);
I = 1.9 <= lg;
scatter(x(I), y(I), 5, 'ro', 'filled');
axis equal
title('Nodes contributing to $d_{i,j} \geq 1.9$');

d = D(:, 2:end) ./ dra; d = d(:);

posr = [xr' yr'];
drar = drf(xr', yr');
[~, D] = knnsearch(posr, posr, 'K', 7);
lg = D(:, end) ./ drar;
I = lg > 1.9;

fo2 = setfig('bo8', [400 400]);
scatter(xr(I), yr(I), 5, 'ro', 'filled');
axis equal

dr = D(:, 2:end) ./ drar; dr = dr(:);


fo2 = setfig('bo2', [400 400]);
histogram(d, 100, 'FaceColor', 'white', 'Normalization', 'probability');
xlabel('$d_{i,j}$')
ylabel('\%')
ytix = get(gca, 'YTick');
set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)
xlim([0 3])
xticks(0:0.5:3)
% title('Nodes placed by fill algorithm')

fo4 = setfig('bo4', [400 400]);
histogram(dr, 100, 'FaceColor', 'white', 'Normalization', 'probability');
xlabel('$d_{i,j}$')
ylabel('\%')
ytix = get(gca, 'YTick');
set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)
xlim([0 3])
xticks(0:0.5:3)

fo5 = setfig('bo5', [400 400]);
dr = ones(size(dr));
histogram(dr, 30, 'FaceColor', 'white', 'Normalization', 'probability');
xlabel('$d_{i,j}$')
ylabel('\%')
ytix = get(gca, 'YTick');
set(gca, 'YTick',ytix, 'YTickLabel',ytix*100)
xlim([0 3])
ylim([0 1.05])
xticks(0:0.5:3)

% exportf(fo5, [imagepath 'hist_ideal.pdf']);
% exportf(fo2, [imagepath 'hist_fill.pdf']);
% exportf(fo4, [imagepath 'hist_relax.pdf']);

% title('Regularised distribution')

% SECOND ITER
% info = h5info(datafile);
% name = info.Groups(2).Name;
% pts = h5read(datafile, [name '/filled']);
% x = pts(1, :);
% y = pts(2, :);
% N = length(x)
%
%
% g1 = setfig('bo6', figsize);
% scatter(x, y, 1, 'k', 'filled')
% daspect([1 1 1])
% xlim([-3 3])
% ylim([-3 3])
% set(gca,'TickDir','out')
%
% ptsr = h5read(datafile, [name '/relaxed']);
% xr = ptsr(1, :);
% yr = ptsr(2, :);
% Nr = length(xr)
%
% g2 = setfig('bo8', figsize);
% scatter(xr, yr, 2, 'k', 'filled')
% daspect([1 1 1])
% xlim([-3 3])
% ylim([-3 3])
% set(gca,'TickDir','out')
%
% g4 = setfig('bo16', figsize);
% scatter(xr, yr, 3, 'k', 'filled')
% daspect([1 1 1])
% xlim(zoomin1x)
% ylim(zoomin1y)
% set(gca,'TickDir','out')

pause(1)
% exportf(g2, [imagepath 'sample_2nd_iter_full.png'], '-png');
% exportf(g4, [imagepath 'sample_2nd_iter_zoom.png'], '-png');


% f3 = setfig('b3');
% quiver(x, y, xr-x, yr-y)
% daspect([1 1 1])
% title('Difference')
% xlim([0 256])
% ylim([0 256])

% LAST ITER
% info = h5info(datafile);
% name = info.Groups(3).Name;
% pts = h5read(datafile, [name '/filled']);
% x = pts(1, :);
% y = pts(2, :);
% N = length(x)
%
% g1 = setfig('bo7', figsize);
% scatter(x, y, 1, 'k', 'filled')
% daspect([1 1 1])
% xlim([-3 3])
% ylim([-3 3])
% set(gca,'TickDir','out')
%
% ptsr = h5read(datafile, [name '/relaxed']);
% xr = ptsr(1, :);
% yr = ptsr(2, :);
% Nr = length(xr)
%
% g2 = setfig('bo9', figsize);
% scatter(xr, yr, 2, 'k', 'filled')
% daspect([1 1 1])
% xlim([-3 3])
% ylim([-3 3])
% set(gca,'TickDir','out')
%
% g4 = setfig('bo15', figsize);
% scatter(xr, yr, 3, 'k', 'filled')
% daspect([1 1 1])
% xlim(zoomin1x)
% ylim(zoomin1y)
% set(gca,'TickDir','out')
%
% exportf(g2, [imagepath 'sample_last_iter_full.png'], '-png');
% exportf(g4, [imagepath 'sample_last_iter_zoom.png'], '-png');

close all
clear 

datafile = 'fill3.h5';
pts = h5read(datafile, '/0000/filled') / 256;
x = pts(1, :);
y = pts(2, :);
N = length(x)

figsize = [400, 400];
zoomin1x = [0.7 0.9];
zoomin1y = [0.3 0.5];

f1 = setfig('b1', figsize);
scatter(x, y, 1, 'k', 'filled')
daspect([1 1 1])
xlim([0 1])
ylim([0 1])
set(gca,'TickDir','out')

f3 = setfig('b3', figsize);
scatter(x, y, 3, 'k', 'filled')
daspect([1 1 1])
xlim(zoomin1x)
ylim(zoomin1y)
set(gca,'TickDir','out')

ptsr = h5read(datafile, '/0000/relaxed') / 256;
xr = ptsr(1, :);
yr = ptsr(2, :);
Nr = length(xr)

f2 = setfig('b2', figsize);
scatter(xr, yr, 1, 'k', 'filled')
daspect([1 1 1])
xlim([0 1])
ylim([0 1])
set(gca,'TickDir','out')

f4 = setfig('b4', figsize);
scatter(xr, yr, 3, 'k', 'filled')
daspect([1 1 1])
xlim(zoomin1x)
ylim(zoomin1y)
set(gca,'TickDir','out')

imagepath = 'images/';
% exportf(f1, [imagepath 'trui_filled_full.png'], '-png');
% exportf(f3, [imagepath 'trui_filled_zoom.png'], '-png');
% exportf(f2, [imagepath 'trui_relaxed_full.png'], '-png');
% exportf(f4, [imagepath 'trui_relaxed_zoom.png'], '-png');

% SECOND ITER
info = h5info(datafile);
name = info.Groups(2).Name;
pts = h5read(datafile, [name '/filled']) / 256;
x = pts(1, :);
y = pts(2, :);
N = length(x)

figsize = [400, 400];
zoomin1x = [0.7 0.9];
zoomin1y = [0.3 0.5];

g1 = setfig('bo6', figsize);
scatter(x, y, 1, 'k', 'filled')
daspect([1 1 1])
xlim([0 1])
ylim([0 1])
set(gca,'TickDir','out')

ptsr = h5read(datafile, [name '/relaxed']) / 256;
xr = ptsr(1, :);
yr = ptsr(2, :);
Nr = length(xr)

g2 = setfig('bo8', figsize);
scatter(xr, yr, 1, 'k', 'filled')
daspect([1 1 1])
xlim([0 1])
ylim([0 1])
set(gca,'TickDir','out')

g4 = setfig('bo16', figsize);
scatter(xr, yr, 3, 'k', 'filled')
daspect([1 1 1])
xlim(zoomin1x)
ylim(zoomin1y)
set(gca,'TickDir','out')

% exportf(g2, [imagepath 'trui_2nd_iter_full.png'], '-png');
% exportf(g4, [imagepath 'trui_2nd_iter_zoom.png'], '-png');


% f3 = setfig('b3');
% quiver(x, y, xr-x, yr-y)
% daspect([1 1 1])
% title('Difference')
% xlim([0 256])
% ylim([0 256])

% LAST ITER
info = h5info(datafile);
name = info.Groups(3).Name;
pts = h5read(datafile, [name '/filled']) / 256;
x = pts(1, :);
y = pts(2, :);
N = length(x)

figsize = [400, 400];
zoomin1x = [0.7 0.9];
zoomin1y = [0.3 0.5];

g1 = setfig('bo7', figsize);
scatter(x, y, 1, 'k', 'filled')
daspect([1 1 1])
xlim([0 1])
ylim([0 1])
set(gca,'TickDir','out')

ptsr = h5read(datafile, [name '/relaxed']) / 256;
xr = ptsr(1, :);
yr = ptsr(2, :);
Nr = length(xr)

g2 = setfig('bo9', figsize);
scatter(xr, yr, 1, 'k', 'filled')
daspect([1 1 1])
xlim([0 1])
ylim([0 1])
set(gca,'TickDir','out')

g4 = setfig('bo15', figsize);
scatter(xr, yr, 3, 'k', 'filled')
daspect([1 1 1])
xlim(zoomin1x)
ylim(zoomin1y)
set(gca,'TickDir','out')



% exportf(g2, [imagepath 'trui_last_iter_full.png'], '-png');
% exportf(g4, [imagepath 'trui_last_iter_zoom.png'], '-png');

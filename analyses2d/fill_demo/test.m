datafile = 'fill_wip.h5';
pts = h5read(datafile, '/0005/filled') / 256;
x = pts(1, :);
y = pts(2, :);

data = [x; y]';
[I, D] = knnsearch(data, data, 'K', 2);
d = sqrt(D(:, 2));

% f1 = setfig('b1');
clf
[X, Y] = meshgrid(linspace(0, 1, 500), linspace(0, 1, 500));
G = griddata(x, y, d, X, Y, 'nearest');
% contourf(X, Y, G, 256, 'EdgeColor', 'none');
scatter(x, y, 1, d);
colormap gray
axis equal
colorbar
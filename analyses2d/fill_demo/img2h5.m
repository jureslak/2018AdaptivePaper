M = imread('trui.png');

pos = zeros(numel(M), 2);
val = zeros(numel(M), 1);

[w, h] = size(M);
for i = 1:w
    for j = 1:h
        pos((i-1)*h + j, :) = [j h-i+1];
        val((i-1)*h + j) = M(i, j);
    end
end

datafile = 'image.h5';
delete(datafile)
h5create(datafile, '/pos', size(pos));
h5write(datafile, '/pos', pos);
h5create(datafile, '/val', length(val));
h5write(datafile, '/val', val);
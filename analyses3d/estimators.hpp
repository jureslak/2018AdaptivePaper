#ifndef COMPRESSED_DISK_ESTIMATORS_HPP
#define COMPRESSED_DISK_ESTIMATORS_HPP

#include <medusa/Medusa_fwd.hpp>

class DeviationEstimator {
    template < typename values_t>
    double get_deviation(const Range<int>& ind, const values_t& solution) {
        double mu = 0;
        for (int i : ind) {
            mu += solution[i];
        }
        mu /= ind.size();

        double var = 0;
        for (int i : ind) {
            var += (solution[i] - mu)*(solution[i] - mu);
        }
        return std::sqrt(var);
    }
  public:

    /**
     * Returns an estimate of error for each domain node.
     * @param domain The discretization of the domain.
     * @param solution Approximated solution in domain nodes.
     * @return An approximation of error in domain nodes.
     */
    template<typename domain_t>
    VectorXd estimate(const domain_t& domain, const std::pair<VectorField3d, VectorField<double, 6>>& sol) {
        auto stress = sol.second;
        int N = domain.size();
        VectorXd error = VectorXd::Zero(N);

        assert_msg(N == stress.rows(), "Solution size %d must match domain size %d.", stress.rows(), N);
        int d = stress.cols();

        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < d; ++j) {
                error(i) += get_deviation(domain.support(i), stress.c(j));
            }
        }
        return error;
    }
};

#endif //COMPRESSED_DISK_ESTIMATORS_HPP

N = 7289;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 831.564;
575 nodes refined, 0 derefined, 0 hit derefine limit, 6714 left same.
t.duration("start", "end") = 1.00207;
N = 7827;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 7271.78;
2042 nodes refined, 0 derefined, 0 hit derefine limit, 5785 left same.
t.duration("start", "end") = 1.07125;
N = 9479;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 4207.37;
2773 nodes refined, 0 derefined, 0 hit derefine limit, 6706 left same.
t.duration("start", "end") = 1.30654;
N = 11927;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 13412.9;
4930 nodes refined, 0 derefined, 0 hit derefine limit, 6997 left same.
t.duration("start", "end") = 1.68574;
N = 15039;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 7447.47;
7663 nodes refined, 0 derefined, 0 hit derefine limit, 7376 left same.
t.duration("start", "end") = 2.23129;
N = 20225;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 11390.1;
12045 nodes refined, 0 derefined, 0 hit derefine limit, 8180 left same.
t.duration("start", "end") = 3.26186;
N = 26520;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 11805.4;
17449 nodes refined, 0 derefined, 0 hit derefine limit, 9071 left same.
t.duration("start", "end") = 4.35705;
N = 34764;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 7451.66;
24843 nodes refined, 0 derefined, 0 hit derefine limit, 9921 left same.
t.duration("start", "end") = 6.57068;
N = 47767;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 14443.3;
36658 nodes refined, 0 derefined, 0 hit derefine limit, 11109 left same.
t.duration("start", "end") = 10.138;
N = 61333;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 2864.87;
49236 nodes refined, 0 derefined, 0 hit derefine limit, 12097 left same.
t.duration("start", "end") = 14.5527;
N = 103117;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 7581.04;
89841 nodes refined, 0 derefined, 0 hit derefine limit, 13276 left same.
t.duration("start", "end") = 29.3039;
N = 138632;
"shapes" = shapes;
"matrix" = matrix;
"compute" = compute;
"solve" = solve;
error.maxCoeff() = 3627.43;
124155 nodes refined, 0 derefined, 0 hit derefine limit, 14477 left same.
t.duration("start", "end") = 48.8294;
t.duration("begin", "end") = 125.715;


./build/adaptive_3d params/adaptive_3d.xml 2> output_adaptive_3d.txt  378,73s user 7,92s system 307% cpu 2:05,84 total
